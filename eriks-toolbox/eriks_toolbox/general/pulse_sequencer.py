# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from scipy import signal
import numpy as np


# +
class PulseSequence:

    def __init__(self, length=10, y_spacing=1, ax=None):

        if not ax:
            fig = plt.figure(figsize=(length, y_spacing))
            self.ax = fig.add_subplot()
        else:
            self.ax = ax

        self.num_channels = 0
        self.length = length
        self.y_spacing = y_spacing

        self.ax.tick_params(length=0)

        self.channels = {}
        self.channel_mapping = {}

    def hide_axes(self, h=True, v=True):

        if h:
            self.ax.get_xaxis().set_ticks([])
        if v:
            self.ax.get_yaxis().set_ticks([])

    def hide_borders(self, t=True, b=True, l=True, r=True):

        self.ax.spines['top'].set_visible(not t)
        self.ax.spines['right'].set_visible(not r)
        self.ax.spines['bottom'].set_visible(not b)
        self.ax.spines['left'].set_visible(not l)

    def add_channel(self, label, color='k'):

        new_channel = Channel(label, self.num_channels, color)
        self.channels[self.num_channels] = new_channel
        self.channel_mapping[label] = self.num_channels
        self.num_channels += 1

        self.ax.set_ylim(0, (self.num_channels + 0.5) * self._y_spacing)

        return new_channel

    def draw_all_channels(self, label=True, font='serif', fontsize=20):

        self.ax.get_yaxis().set_ticks([])

        for channel in self.channels.values():

            channel.draw(self, label=label, font=font, fontsize=fontsize)

    def add_pulse(
        self,
        channel,
        t0,
        duration,
        amplitude,
        shape='square',
        label='',
        arrow=False,
        arrow_dir='h',
        color=None,
    ):

        new_pulse = Pulse(t0, duration, amplitude, shape, label, arrow,
                          arrow_dir, color)

        if isinstance(channel, str):
            ch = self.channels[self.channel_mapping[channel]]
        else:
            ch = self.channels[channel]

        ch.pulses[ch.num_pulses] = new_pulse
        ch.num_pulses += 1

        return new_pulse

    def draw_all_pulses(self,
                        label=True,
                        arrow=True,
                        font='serif',
                        fontsize=20):

        for channel in self.channels.values():
            for pulse in channel.pulses.values():

                if not pulse.color:
                    color = channel.color
                else:
                    color = pulse.color

                pulse.draw(self,
                           channel,
                           label=label,
                           arrow=arrow,
                           c=color,
                           font=font,
                           fontsize=fontsize)

    @property
    def length(self):
        return self._length

    @length.setter
    def length(self, new_length):
        self._length = new_length
        self.ax.set_xlim(0, new_length)

    @property
    def y_spacing(self):
        return self._y_spacing

    @y_spacing.setter
    def y_spacing(self, new_y_spacing):
        self._y_spacing = new_y_spacing
        self.ax.set_ylim(0, (self.num_channels + 0.5) * new_y_spacing)


# -


class Channel:

    def __init__(self, label, index, color='k'):

        self.label = label
        self.index = index
        self.color = color

        self.num_pulses = 0

        self.pulses = {}

    def draw(self,
             sequence,
             c='k',
             lw=1,
             ls=(0, (5, 5)),
             label=True,
             font='serif',
             fontsize=20):

        ax = sequence.ax
        y_spacing = sequence.y_spacing
        length = sequence.length

        y = (0.25 + self.index) * y_spacing

        ax.hlines(
            y,
            0,
            length,
            color=c,
            linewidth=lw,
            linestyle=ls,
        )

        if label:

            yticks = ax.get_yticks()
            ytick_labels = ax.get_yticklabels()

            ax.set_yticks([y, *yticks], [self.label, *ytick_labels],
                          fontname=font,
                          fontsize=fontsize)


class Pulse:

    def __init__(self,
                 t0,
                 duration,
                 amplitude,
                 shape='square',
                 label='',
                 arrow=False,
                 arrow_dir='h',
                 color=None):

        self.t0 = t0
        self.duration = duration
        self.amplitude = amplitude
        self.shape = shape
        self.label = label
        self.arrow = arrow
        self.arrow_dir = arrow_dir
        self.color = color

        if shape == 'square':

            omega = 2 * np.pi / (2 * duration)
            self.x = np.linspace(-duration * 0.01,
                                 duration * 1.01,
                                 100,
                                 endpoint=True)
            self.y = amplitude / 2 * signal.square(omega * self.x)

        if shape == 'gaussian':

            self.x = np.linspace(-duration * 0.01,
                                 duration * 1.01,
                                 100,
                                 endpoint=True)
            self.y = amplitude * signal.gaussian(100, std=100 / 5)

    def draw(self,
             sequence,
             channel,
             c=None,
             lw=3,
             ls='-',
             label=True,
             font='serif',
             fontsize=20,
             arrow=False):

        ax = sequence.ax
        y_spacing = sequence.y_spacing
        offset = (0.25 + channel.index) * y_spacing

        if not c:
            c = channel.color

        if self.shape == 'square':

            ax.plot(
                self.x + self.t0,
                self.y + offset + self.amplitude / 2,
                color=c,
                linewidth=lw,
                linestyle=ls,
            )

        if self.shape == 'gaussian':

            ax.plot(
                self.x + self.t0,
                self.y - self.y[0] + offset,
                color=c,
                linewidth=lw,
                linestyle=ls,
            )

        if self.shape == 'idle':

            ...

        if arrow and self.arrow:

            if self.arrow_dir in ['H', 'h']:
                start = (self.t0 + 0.05, self.amplitude / 2 + offset)
                stop = (self.t0 + self.duration - 0.05,
                        self.amplitude / 2 + offset)

            elif self.arrow_dir in ['V', 'v']:
                start = (self.t0 + self.duration / 2, offset + 0.025)
                stop = (self.t0 + self.duration / 2, self.amplitude + offset -
                        0.05 - (0.025 if self.shape == "gaussian" else 0))

            arrow_patch = mpatches.FancyArrowPatch(
                start,
                stop,
                arrowstyle='|-|, widthA=.14, widthB=.14',
                mutation_scale=20,
                lw=lw,
            )
            ax.add_patch(arrow_patch)

        if label and self.label:

            self.draw_label(sequence,
                            channel,
                            self.label, (0, self.amplitude * .25),
                            font=font,
                            fontsize=fontsize)

    def draw_label(self,
                   sequence,
                   channel,
                   label,
                   offset,
                   font='serif',
                   fontsize=20):

        dx, dy = offset

        ax = sequence.ax
        y_spacing = sequence.y_spacing
        offset = (0.25 + channel.index) * y_spacing

        ax.annotate(
            label,
            xy=(self.t0 + self.duration / 2, offset + self.amplitude),
            xytext=(self.t0 + self.duration / 2 + dx,
                    offset + self.amplitude + dy),
            ha='center',
            font=font,
            fontsize=fontsize,
        )
