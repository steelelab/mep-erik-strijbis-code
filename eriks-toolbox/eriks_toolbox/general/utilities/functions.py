from typing import Iterable, Union

import matplotlib.pyplot as plt
import numpy as np
from qualang_tools.analysis.discriminator import _false_detections
from scipy.optimize import minimize
from scipy.signal import savgol_filter


def bool_to_sign(bools: Union[bool, Iterable[bool]]):
    if isinstance(bools, (bool, np.ndarray)):
        return 2 * bools - 1

    if isinstance(bools, list):
        return [2 * b - 1 for b in bools]


def is_linear_sequence(sequence):
    differences = np.diff(listify(sequence))

    return len(set(differences)) == 1


def is_log_sequence(sequence):
    ratios = np.divide(listify(sequence)[1:], listify(sequence)[:-1])

    return len(set(ratios)) == 1


def listify(value):
    """function used to ensure that type(value) == list, only if type(value) == Iterable"""

    if not isinstance(value, Iterable):
        return [value]

    elif isinstance(value, np.ndarray) and value.size == 1:
        temp_list = value.tolist()
        if isinstance(temp_list, list):
            return temp_list
        return [temp_list]

    else:
        return list(value)


def replace_strings(dict_: dict, old_str: str, new_str: str):
    """function used to replace all occurrences of a certain old_str a dict by another new_str
    Args:
        dict_ (dict): dictionary to format
        old_str: string to replace
        new_str (str): new string
    """

    if isinstance(dict_, dict):
        return {
            replace_strings(k, old_str, new_str):
            replace_strings(v, old_str, new_str)
            for k, v in dict_.items()
        }

    if isinstance(dict_, list):
        return [replace_strings(value, old_str, new_str) for value in dict_]

    elif isinstance(dict_, str):
        if not isinstance(new_str, str):
            return new_str if dict_ == old_str else dict_

        return dict_.replace(old_str, new_str)

    return dict_


def rgetattr(obj, attr):
    """tool for recursively getting attributes. Also works for dict"""

    temp_attr = attr.split(".")

    if len(temp_attr) > 1:
        if isinstance(obj, dict):
            return rgetattr(obj.get(temp_attr[0], {}), ".".join(temp_attr[1:]))

        return rgetattr(getattr(obj, temp_attr[0]), ".".join(temp_attr[1:]))

    if isinstance(obj, dict):
        return obj.get(temp_attr[0])
    return getattr(obj, temp_attr[0])


def rsetattr(obj, attr, value):
    """tool for recursively setting attributes. Also works for dict"""

    temp_attr = attr.split(".")

    if len(temp_attr) > 1:
        if isinstance(obj, dict):
            return rsetattr(obj.setdefault(temp_attr[0], {}),
                            ".".join(temp_attr[1:]), value)

        return rsetattr(getattr(obj, temp_attr[0]), ".".join(temp_attr[1:]),
                        value)

    if isinstance(obj, dict):
        obj[temp_attr[0]] = value
    else:
        setattr(obj, temp_attr[0], value)


def savgol_window_optimizer(x, variance, polyorder: int = 2):
    """savitzky-golay filter implemented as described in https://arxiv.org/pdf/1808.10489.pdf
    Args:
        x (Iterable): input values
        polyorder (int): polynomial order of filter
    Returns:
        optimal_window_length (int)
    """

    # optimal_window_length = len(x)
    optimal_window_length = polyorder + 2
    control_window_length = polyorder

    window_lengths = np.array([])
    duplicates = np.array([])

    coefficient_0 = 2 * (polyorder + 2) * np.math.factorial(
        2 * polyorder + 3)**2 / np.math.factorial(polyorder + 1)**2

    while optimal_window_length != control_window_length:

        if optimal_window_length in duplicates:
            return int(np.average(duplicates))

        elif optimal_window_length in window_lengths:
            duplicates = np.append(duplicates, optimal_window_length)

        else:
            window_lengths = np.append(window_lengths, optimal_window_length)

        control_window_length = int(optimal_window_length)

        smoothed_x = savgol_filter(x, control_window_length, polyorder)

        smoothed_gradient_smoothed_x = savgol_filter(
            np.gradient(smoothed_x),
            control_window_length,
            polyorder=polyorder,
        )

        triple_gradient = np.gradient(
            np.gradient(np.gradient(smoothed_gradient_smoothed_x)))

        coefficient_1 = np.mean(triple_gradient**2)

        optimal_window_length = int((coefficient_0 * variance /
                                     coefficient_1)**(1 / (2 * polyorder + 5)))

        if optimal_window_length <= polyorder:
            return int(polyorder + 1)

        elif optimal_window_length > len(x):
            return len(x)

    return int(optimal_window_length)


def three_state_discriminator(Ig,
                              Qg,
                              Ie,
                              Qe,
                              If,
                              Qf,
                              b_print=True,
                              b_plot=True):

    ge_angle = np.arctan2(np.mean(Qe) - np.mean(Qg), np.mean(Ig) - np.mean(Ie))
    ef_angle = np.arctan2(np.mean(Qf) - np.mean(Qe), np.mean(Ie) - np.mean(If))
    fg_angle = np.arctan2(np.mean(Qg) - np.mean(Qf), np.mean(If) - np.mean(Ig))

    ge_angle += (np.pi * (np.mean((Ig - Ie) * np.cos(ge_angle) -
                                  (Qg - Qe) * np.sin(ge_angle)) > 0))

    ef_angle += (np.pi * (np.mean((Ie - If) * np.cos(ef_angle) -
                                  (Qe - Qf) * np.sin(ef_angle)) > 0))

    fg_angle += (np.pi * (np.mean((If - Ig) * np.cos(fg_angle) -
                                  (Qf - Qg) * np.sin(fg_angle)) > 0))

    Ig_ge = Ig * np.cos(ge_angle) - Qg * np.sin(ge_angle)
    Qg_ge = Ig * np.sin(ge_angle) + Qg * np.cos(ge_angle)
    Ig_ef = Ig * np.cos(ef_angle) - Qg * np.sin(ef_angle)
    # Qg_ef = Ig * np.sin(ef_angle) + Qg * np.cos(ef_angle)
    Ig_fg = Ig * np.cos(fg_angle) - Qg * np.sin(fg_angle)
    # Qg_fg = Ig * np.sin(fg_angle) + Qg * np.cos(fg_angle)

    Ie_ge = Ie * np.cos(ge_angle) - Qe * np.sin(ge_angle)
    Qe_ge = Ie * np.sin(ge_angle) + Qe * np.cos(ge_angle)
    Ie_ef = Ie * np.cos(ef_angle) - Qe * np.sin(ef_angle)
    # Qe_ef = Ie * np.sin(ef_angle) + Qe * np.cos(ef_angle)
    Ie_fg = Ie * np.cos(fg_angle) - Qe * np.sin(fg_angle)
    # Qe_fg = Ie * np.sin(fg_angle) + Qe * np.cos(fg_angle)

    If_ge = If * np.cos(ge_angle) - Qf * np.sin(ge_angle)
    Qf_ge = If * np.sin(ge_angle) + Qf * np.cos(ge_angle)
    If_ef = If * np.cos(ef_angle) - Qf * np.sin(ef_angle)
    # Qf_ef = If * np.sin(ef_angle) + Qf * np.cos(ef_angle)
    If_fg = If * np.cos(fg_angle) - Qf * np.sin(fg_angle)
    # Qf_fg = If * np.sin(fg_angle) + Qf * np.cos(fg_angle)

    ge_threshold = minimize(
        _false_detections,
        0.5 * (np.mean(Ig_ge) + np.mean(Ie_ge)),
        (Ig_ge, Ie_ge),
        method="Nelder-Mead",
    ).x[0]

    ef_threshold = minimize(
        _false_detections,
        0.5 * (np.mean(Ie_ef) + np.mean(If_ef)),
        (Ie_ef, If_ef),
        method="Nelder-Mead",
    ).x[0]

    fg_threshold = minimize(
        _false_detections,
        0.5 * (np.mean(If_fg) + np.mean(Ig_fg)),
        (If_fg, Ig_fg),
        method="Nelder-Mead",
    ).x[0]

    gg = np.sum((Ig_ge < ge_threshold) & (Ig_fg > fg_threshold)) / len(Ig)
    ge = np.sum((Ig_ge > ge_threshold) & (Ig_ef < ef_threshold)) / len(Ig)
    gf = np.sum((Ig_ef > ef_threshold) & (Ig_fg < fg_threshold)) / len(Ig)

    eg = np.sum((Ie_ge < ge_threshold) & (Ie_fg > fg_threshold)) / len(Ie)
    ee = np.sum((Ie_ge > ge_threshold) & (Ie_ef < ef_threshold)) / len(Ie)
    ef = np.sum((Ie_ef > ef_threshold) & (Ie_fg < fg_threshold)) / len(Ie)

    fg = np.sum((If_ge < ge_threshold) & (If_fg > fg_threshold)) / len(If)
    fe = np.sum((If_ge > ge_threshold) & (If_ef < ef_threshold)) / len(If)
    ff = np.sum((If_ef > ef_threshold) & (If_fg < fg_threshold)) / len(If)

    fidelity = 100 * (gg + ee + ff) / 3

    if b_print:
        print(f"""
        Fidelity Matrix:
        ----------------------------------
        | {gg:.3f} | {ge:.3f} | {gf:.3f} |
        ----------------------------------
        | {eg:.3f} | {ee:.3f} | {ef:.3f} |
        ----------------------------------
        | {fg:.3f} | {fe:.3f} | {ff:.3f} |
        ----------------------------------
        IQ plane rotated by:
            ge: {ge_angle / np.pi:.3f}\u03C0 rad
            ef: {ef_angle / np.pi:.3f}\u03C0 rad
            fg: {fg_angle / np.pi:.3f}\u03C0 rad
        Threshold:
            ge: {ge_threshold:.3e}
            ef: {ef_threshold:.3e}
            fg: {fg_threshold:.3e}
        Fidelity: {fidelity:.1f}%
        """)

    if b_plot:
        # Plot the data
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(8, 8))

        red = [0.19477124, 0.48650519, 0.71872357, 0.75]
        blue = [0.76724337, 0.23252595, 0.23398693, 0.75]

        ax1.scatter(Ig, Qg, marker=".", alpha=0.1, label="|g⟩", color=blue)
        ax1.scatter(Ie, Qe, marker=".", alpha=0.1, label="|e⟩", color=red)
        ax1.scatter(If,
                    Qf,
                    marker=".",
                    alpha=0.1,
                    label="|e⟩",
                    color='tab:green')
        ax1.axis("equal")
        ax1.set_xlabel("I (V)")
        ax1.set_ylabel("Q (V)")
        ax1.set_title("Original Single Shot Data")
        ax1.legend(loc="upper right")

        ax2.scatter(
            Ig_ge,
            Qg_ge,
            marker=".",
            alpha=0.1,
            label="|g⟩",
            color=blue,
        )
        ax2.scatter(
            Ie_ge,
            Qe_ge,
            marker=".",
            alpha=0.1,
            label="|e⟩",
            color=red,
        )
        ax2.scatter(
            If_ge,
            Qf_ge,
            marker=".",
            alpha=0.1,
            label="|f⟩",
            color='tab:green',
        )
        ax2.axis("equal")
        ax2.set_xlabel("I (V)")
        ax2.set_ylabel("Q (V)")
        ax2.set_title(
            f"Rotated Single Shot Data ({ge_angle / np.pi:.3f}π rad)")
        ax2.legend(loc="upper right")

        ax3.hist(Ig_ge, bins=50, alpha=0.75, label="|g⟩", color=blue)
        ax3.hist(Ie_ge, bins=50, alpha=0.75, label="|e⟩", color=red)
        ax3.hist(If_ge, bins=50, alpha=0.75, label="|f⟩", color='tab:green')
        ax3.axvline(x=ge_threshold,
                    color="k",
                    ls="--",
                    alpha=0.5,
                    label=f"ge: {ge_threshold/1e-3:.2g} mV")
        ax3.set_xlabel("I (V)")
        ax3.set_title("1D Histogram")
        ax3.legend(loc="lower right")

        ax4.imshow(np.array([[gg, ge, gf], [eg, ee, ef], [fg, fe, ff]]),
                   cmap="RdBu_r")
        ax4.set_xticks([0, 1, 2])
        ax4.set_yticks([0, 1, 2])
        ax4.set_xticklabels(labels=["|g>", "|e>", "|f>"])
        ax4.set_yticklabels(labels=["|g>", "|e>", "|f>"])
        ax4.set_ylabel("Prepared")
        ax4.set_xlabel("Measured")
        ax4.text(0, 0, f"{100 * gg:.1f}%", ha="center", va="center", color="k")
        ax4.text(1, 0, f"{100 * ge:.1f}%", ha="center", va="center", color="w")
        ax4.text(2, 0, f"{100 * gf:.1f}%", ha="center", va="center", color="w")
        ax4.text(0, 1, f"{100 * eg:.1f}%", ha="center", va="center", color="w")
        ax4.text(1, 1, f"{100 * ee:.1f}%", ha="center", va="center", color="k")
        ax4.text(2, 1, f"{100 * ef:.1f}%", ha="center", va="center", color="w")
        ax4.text(0, 2, f"{100 * fg:.1f}%", ha="center", va="center", color="w")
        ax4.text(1, 2, f"{100 * fe:.1f}%", ha="center", va="center", color="w")
        ax4.text(2, 2, f"{100 * ff:.1f}%", ha="center", va="center", color="k")
        ax4.set_title("Fidelities")

        fig.tight_layout()

    angles = (ge_angle, ef_angle, fg_angle)
    thresholds = (ge_threshold, ef_threshold, fg_threshold)
    matrix_elements = (gg, ge, gf, eg, ee, ef, fg, fe, ff)

    return angles, thresholds, fidelity, matrix_elements
