from typing import Union

import numpy as np


def v_to_dbm(v: Union[int, float, np.ndarray, list], z0=50):
    if isinstance(v, list):
        return [20 * np.log10(v_ / np.sqrt(z0 * 2 * 0.001)) for v_ in v]

    elif isinstance(v, (int, float, np.ndarray)):
        return 20 * np.log10(v / np.sqrt(z0 * 2 * 0.001))

    raise TypeError


def dbm_to_v(dbm: Union[int, float, np.ndarray, list], z0=50):
    if isinstance(dbm, list):
        return [10**(dbm_ / 20) * np.sqrt(z0 * 2 * 0.001) for dbm_ in dbm]

    elif isinstance(dbm, (int, float, np.ndarray)):
        return 10**(dbm / 20) * np.sqrt(z0 * 2 * 0.001)

    raise TypeError


def x_to_db(x: Union[int, float, np.ndarray, list]):
    if isinstance(x, list):
        return [20 * np.log10(x_) for x_ in x]

    elif isinstance(x, (int, float, np.ndarray)):
        return 20 * np.log10(x)

    raise TypeError


def db_to_x(db: Union[int, float, np.ndarray, list]):
    if isinstance(db, list):
        return [10**(db_ / 20) for db_ in db]

    elif isinstance(db, (int, float, np.ndarray)):
        return 10**(db / 20)

    raise TypeError
