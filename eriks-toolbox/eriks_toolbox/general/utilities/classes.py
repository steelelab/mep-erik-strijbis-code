# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#notebook-for-useful-classes-/-functions-/-snippets" data-toc-modified-id="notebook-for-useful-classes-/-functions-/-snippets-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>notebook for useful classes / functions / snippets</a></span></li></ul></div>

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#notebook-for-useful-classes-/-functions-/-snippets" data-toc-modified-id="notebook-for-useful-classes-/-functions-/-snippets-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>notebook for useful classes / functions / snippets</a></span></li></ul></div>

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#notebook-for-useful-classes-/-functions-/-snippets" data-toc-modified-id="notebook-for-useful-classes-/-functions-/-snippets-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>notebook for useful classes / functions / snippets</a></span></li></ul></div>

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#notebook-for-useful-classes-/-functions-/-snippets" data-toc-modified-id="notebook-for-useful-classes-/-functions-/-snippets-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>notebook for useful classes / functions / snippets</a></span></li></ul></div>

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#notebook-for-useful-classes-/-functions-/-snippets" data-toc-modified-id="notebook-for-useful-classes-/-functions-/-snippets-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>notebook for useful classes / functions / snippets</a></span></li></ul></div>

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#notebook-for-useful-classes-/-functions-/-snippets" data-toc-modified-id="notebook-for-useful-classes-/-functions-/-snippets-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>notebook for useful classes / functions / snippets</a></span></li></ul></div>

# ### notebook for useful classes / functions / snippets
#
# by Erik

# +
import os
import sys
from dataclasses import dataclass
from typing import Iterable

import numpy as np
from eriks_stlab_toolbox.general.utilities.functions import savgol_window_optimizer
from scipy.signal import savgol_filter

# -


class HiddenPrints:
    """suppress print statements in a section:

    with HiddenPrints():

        <insert code>

    """

    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, "w")

    def __exit__(self, *args):
        sys.stdout.close()
        sys.stdout = self._original_stdout


@dataclass
class Residuals:
    data: Iterable
    fits: Iterable
    par_num: int = 0

    @property
    def fitted(self):
        return self.fits

    @property
    def actual_avg(self):
        return sum(self.data) / len(self.data)

    @property
    def target_avg(self):
        return sum(self.fits) / len(self.fits)

    @property
    def residuals(self):
        return self.data - self.fits

    @property
    def residual_avg(self):
        return sum(self.residuals) / len(self.residuals)

    @property
    def relative_residuals(self):
        return self.residuals / self.fits

    @property
    def standard_deviation(self):
        return np.std(self.residuals, ddof=self.par_num)

    @property
    def std(self):
        return self.standard_deviation

    @property
    def sum_of_squared_errors(self):
        return np.sum(self.residuals**2)

    @property
    def total_sum_of_squares(self):
        return np.sum((self.data - np.mean(self.data))**2)

    @property
    def mean_squared_error(self):
        return self.sum_of_squared_errors / (len(self.data) - self.par_num)

    @property
    def root_mean_squared_error(self):
        return np.sqrt(self.mean_squared_error)

    @property
    def rmse(self):
        return self.root_mean_squared_error

    @property
    def r_squared(self):
        return 1 - self.sum_of_squared_errors / self.total_sum_of_squares

    @property
    def R2(self):
        return self.r_squared

    @property
    def adjusted_r_squared(self):
        return 1 - (self.sum_of_squared_errors *
                    (len(self.data) - 1)) / (self.total_sum_of_squares *
                                             (len(self.data) - self.par_num))

    @property
    def adjusted_R2(self):
        return self.adjusted_r_squared


@dataclass
class SignalSmoother:
    signal: complex
    filter_order: int = 2
    outlier_factor: int = 2

    @property
    def average(self):

        stds = np.std(self.signal, axis=1)
        return np.array([
            np.average(s[np.abs(s - np.average(s)) <= (self.outlier_factor *
                                                       std)])
            for s, std in zip(self.signal, stds)
        ])

    @property
    def variance(self):

        stds = np.std(self.signal, axis=1)
        return np.array([
            np.var(s[np.abs(s - np.average(s)) <= (self.outlier_factor * std)])
            for s, std in zip(self.signal, stds)
        ])

    @property
    def filter_window_size(self):

        return savgol_window_optimizer(
            np.abs(self.average),
            np.average(self.variance),
            polyorder=self.filter_order,
        )

    @property
    def smoothed_signal(self):

        return savgol_filter(
            self.average,
            self.filter_window_size,
            polyorder=self.filter_order,
        )

    @property
    def smoothed_variance(self):

        return savgol_filter(
            self.variance,
            self.filter_window_size,
            polyorder=self.filter_order,
        )
