from __future__ import annotations

from dataclasses import dataclass, field
from typing import Iterable, Union

import numpy as np

FloatOrIterable = Union[float, Iterable[float]]
FloatOrInt = Union[float, int]


@dataclass
class Log:
    log_name: str = ""
    comment: str = ""
    project: str = ""
    user: str = ""
    tags: list = field(default_factory=list)

    def __call__(self):
        return {
            "log_name": self.log_name,
            "comment": self.comment,
            "tags": {
                "project": self.project,
                "user": self.user,
                "tags": self.tags,
            },
        }

    def __repr__(self):
        txt = "\n".join([
            f"# LOG_NAME: {self.log_name}",
            f"# PROJECT: {self.project}",
            f"# USER: {self.user}",
            f"# TAGS: {self.tags}",
            f"# COMMENT: {self.comment}",
        ])

        return txt

    @classmethod
    def from_dict(self, log_settings_dict: Union[dict, Log]):
        if isinstance(log_settings_dict, dict):
            return Log(**log_settings_dict)
        elif isinstance(self.log_settings, Log):
            return log_settings_dict
        else:
            raise ValueError("please provide LOG SETTINGS in correct format")


@dataclass
class Mix:
    """this dataclass contains mixer properties for mixing pulses using IQ mixers
    Args:
        lo_power (float, Iterable[float]): lo power to use for up- and downconversion
        if_min (float): mininum if frequency to use
        if_max (float): maximum if frequency to use
    """

    lo_power: FloatOrIterable = 1e-3  # V
    if_min: FloatOrInt = 5e6  # Hz
    if_max: FloatOrInt = 195e6  # Hz

    def __post_init__(self):
        if isinstance(self.lo_power,
                      Iterable) and not isinstance(self.lo_power, np.ndarray):
            self.lo_power = np.array(self.lo_power)

        elif not isinstance(self.lo_power, Iterable):
            self.lo_power = np.array([self.lo_power])

    @classmethod
    def from_dict(self, mix_settings_dict: Union[dict, Mix]):
        if isinstance(mix_settings_dict, dict):
            return Mix(**mix_settings_dict)
        elif isinstance(mix_settings_dict, Mix):
            return mix_settings_dict
        else:
            raise ValueError("please provide MIX SETTINGS in correct format")


@dataclass
class Pulse:
    """this dataclass contains pulse properties for an arbitrary pulse
    Args
        angle (float): in radians, rotates the pulse or readout weights in IQ plane
        frequency (float, Iterable[float]): frequency to measure at
        power (float, Iterable[float]): IF power to measure at
        shape (str): 'square', 'gaussian', etcc
        width (float, Iterable[float]): width of readout pulse. also: n_samples = pulse_width * (500 MSa/s)
    """

    angle: float = 0
    frequency: FloatOrIterable = field(default_factory=np.linspace(
        4e9, 8e9, 401))  # Hz
    path_to_integration_weights: str = None
    power: FloatOrIterable = 1e-3  # V
    shape: str = "square"
    width: FloatOrIterable = 1e-6

    def __post_init__(self):
        for variable in ["frequency", "power", "width"]:
            variable_ = getattr(self, variable)

            if isinstance(variable_,
                          Iterable) and not isinstance(variable_, np.ndarray):
                variable_ = np.array(variable_)

            elif not isinstance(variable_, Iterable):
                variable_ = np.array([variable_])

            setattr(self, variable, variable_)

    @classmethod
    def from_dict(self, pulse_settings_dict: Union[dict, Pulse]):
        if isinstance(pulse_settings_dict, dict):
            return Pulse(**pulse_settings_dict)
        elif isinstance(pulse_settings_dict, Pulse):
            return pulse_settings_dict
        else:
            raise ValueError("please provide PULSE SETTINGS in correct format")


@dataclass
class Sweep:
    """this dataclass contains general spectroscopy settings
    Args:
        n_averages (int): number of averages per sweep
        overlapping_readout (bool): if False, put readout pulse after pulse sequence
        time_of_flight (int): pulse travel time between send and receive
        wait_between (float): time to wait between pulse sequences
    """

    idle_time: int = 0  # s
    n_single_shots: int = 100
    n_averages: int = 100
    time_of_flight: float = 0  # s
    wait_between: FloatOrInt = 1e-6  # s
    overlapping_readout: bool = False

    @classmethod
    def from_dict(self, sweep_settings_dict: Union[dict, Sweep]):
        if isinstance(sweep_settings_dict, dict):
            return Sweep(**sweep_settings_dict)
        elif isinstance(sweep_settings_dict, Pulse):
            return sweep_settings_dict
        else:
            raise ValueError("please provide SWEEP SETTINGS in correct format")
