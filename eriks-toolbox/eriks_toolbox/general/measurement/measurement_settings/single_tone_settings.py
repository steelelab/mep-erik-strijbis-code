from __future__ import annotations

from dataclasses import dataclass
from typing import Union

import numpy as np
from eriks_stlab_toolbox.general.measurement import Mix, Pulse, Sweep
from eriks_stlab_toolbox.general.utilities import rsetattr


@dataclass
class SingleToneSettings:
    """dataclass for sweep settings for a single tone measurement
    Args:
        readout_pulse (Pulse): Pulse object, containing readout pulse data
        readout_mix (Mix): Mix object, containing data for pulse mixing
        readout_sweep (Sweep): Sweep object, containing general sweep variables
        --- see building_blocks.py for reference ---
    """

    readout_pulse: Union[Pulse, dict]
    readout_mix: Union[Mix, dict]
    sweep: Union[Sweep, dict]

    def __post_init__(self):
        self.readout_pulse = Pulse.from_dict(self.readout_pulse)
        self.readout_mix = Mix.from_dict(self.readout_mix)
        self.sweep = Sweep.from_dict(self.sweep)

        # turn frequencies into seperate LO and IF frequencies

        # get number of if_sweep_range that fit in total f sweep range
        if_sweep_range = self.readout_mix.if_max - self.readout_mix.if_min
        n_lo_steps, remainder = divmod(
            (max(self.readout_pulse.frequency) -
             min(self.readout_pulse.frequency)),
            if_sweep_range,
        )

        # adjust if_sweep_range accoriding to total number fitting in it
        temp_adjusted_if_sweep_range = (max(self.readout_pulse.frequency) -
                                        min(self.readout_pulse.frequency)) / (
                                            n_lo_steps + (remainder > 0))

        lo_min = min(self.readout_pulse.frequency) - self.readout_mix.if_min
        if not (np.nan_to_num(temp_adjusted_if_sweep_range)):
            rsetattr(self, "readout_mix.frequency", np.array([lo_min]))
            rsetattr(self, "readout_pulse.frequency",
                     np.array([self.readout_mix.if_min]))

        else:
            # round if_sweep_range so that it is an integer multiple of f_step
            f_step = self.readout_pulse.frequency[
                1] - self.readout_pulse.frequency[0]

            points_per_if_sweep = int(
                np.floor(temp_adjusted_if_sweep_range / f_step)) + 1
            adjusted_if_sweep_range = points_per_if_sweep * f_step

            relative_frequencies = self.readout_pulse.frequency - self.readout_mix.if_min - lo_min
            lo_f, if_f = divmod(relative_frequencies, adjusted_if_sweep_range)

            rsetattr(self, "readout_mix.frequency",
                     lo_min + adjusted_if_sweep_range * lo_f)
            rsetattr(self, "readout_pulse.frequency",
                     self.readout_mix.if_min + if_f)

        # # adjust if_sweep_range accoriding to total number fitting in it
        # temp_adjusted_if_sweep_range = (max(self.readout_pulse.frequency) -
        #                                 min(self.readout_pulse.frequency)) / (
        #                                     n_lo_steps + (remainder > 0))

        # # round if_sweep_range so that it is an integer multiple of f_step
        # f_step = self.readout_pulse.frequency[
        #     1] - self.readout_pulse.frequency[0]

        # points_per_if_sweep = round(
        #     np.floor(temp_adjusted_if_sweep_range / f_step)) + 1
        # adjusted_if_sweep_range = points_per_if_sweep * f_step

        # lo_min = min(self.readout_pulse.frequency) - self.readout_mix.if_min
        # relative_frequencies = self.readout_pulse.frequency - self.readout_mix.if_min - lo_min
        # lo_f, if_f = divmod(relative_frequencies, adjusted_if_sweep_range)

        # self.readout_mix.frequency = lo_min + adjusted_if_sweep_range * lo_f
        # self.readout_pulse.frequency = self.readout_mix.if_min + if_f

    @classmethod
    def from_dict(self, single_tone_settings: Union[dict, SingleToneSettings]):
        if isinstance(single_tone_settings, dict):
            return SingleToneSettings(**single_tone_settings)
        elif isinstance(single_tone_settings, Pulse):
            return single_tone_settings
        else:
            raise ValueError("please provide PULSE SETTINGS in correct format")
