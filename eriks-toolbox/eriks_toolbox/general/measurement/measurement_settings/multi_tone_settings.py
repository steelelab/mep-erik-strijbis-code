from __future__ import annotations

from dataclasses import dataclass
from typing import Union, Optional

import numpy as np
from eriks_stlab_toolbox.general.measurement import Mix, Pulse, Sweep
from eriks_stlab_toolbox.general.utilities import rsetattr


@dataclass
class MultiToneSettings:
    """dataclass for sweep settings for a two tone measurement
    Args:
        readout_pulse (Pulse): Pulse object, containing readout pulse data
        readout_mix (Mix): Mix object, containing data for readout pulse mixing
        drive_pulse (Pulse): Pulse object, containing drive pulse data
        drive_mix (Mix): Mix object, containing data for drive pulse mixing
        sweep (Sweep): Sweep object, containing general sweep variables
        --- see building_blocks.py for reference ---
    """

    readout_pulse: Union[Pulse, dict]
    readout_mix: Union[Mix, dict]
    drive_1_pulse: Optional[Union[Pulse, dict]] = None
    drive_1_mix: Optional[Union[Pulse, dict]] = None
    drive_2_pulse: Optional[Union[Pulse, dict]] = None
    drive_2_mix: Optional[Union[Mix, dict]] = None
    drive_3_pulse: Optional[Union[Pulse, dict]] = None
    drive_3_mix: Optional[Union[Mix, dict]] = None
    drive_4_pulse: Optional[Union[Pulse, dict]] = None
    drive_4_mix: Optional[Union[Mix, dict]] = None
    sweep: Optional[Union[Sweep, dict]] = None

    def __post_init__(self):
        self.readout_pulse = Pulse.from_dict(self.readout_pulse)
        self.readout_mix = Mix.from_dict(self.readout_mix)
        self.sweep = Sweep.from_dict(self.sweep)

        pulse_types = ['readout']

        for i in [1, 2, 3, 4]:
            if pulse := getattr(self, f'drive_{i}_pulse', None):
                setattr(self, f'drive_{i}_pulse', Pulse.from_dict(pulse))
                setattr(self, f'drive_{i}_mix',
                        Mix.from_dict(getattr(self, f'drive_{i}_mix')))
                pulse_types += [f'drive_{i}']

        # turn frequencies into seperate LO and IF frequencies
        for pulse_type in pulse_types:

            mix = getattr(self, f"{pulse_type}_mix")
            pulse = getattr(self, f"{pulse_type}_pulse")

            # get number of if_sweep_range that fit in total f sweep range
            if_sweep_range = mix.if_max - mix.if_min
            n_lo_steps, remainder = divmod(
                (max(pulse.frequency) - min(pulse.frequency)),
                if_sweep_range,
            )

            # adjust if_sweep_range accoriding to total number fitting in it
            temp_adjusted_if_sweep_range = (max(pulse.frequency) - min(
                pulse.frequency)) / (n_lo_steps + (remainder > 0))

            lo_min = min(pulse.frequency) - mix.if_min

            if not (np.nan_to_num(temp_adjusted_if_sweep_range)):
                rsetattr(self, f"{pulse_type}_mix.frequency",
                         np.array([lo_min]))
                rsetattr(self, f"{pulse_type}_pulse.frequency",
                         np.array([mix.if_min]))

            else:
                # round if_sweep_range so that it is an integer multiple of f_step
                f_step = pulse.frequency[1] - pulse.frequency[0]

                points_per_if_sweep = int(
                    np.floor(temp_adjusted_if_sweep_range / f_step)) + 1
                adjusted_if_sweep_range = points_per_if_sweep * f_step

                relative_frequencies = pulse.frequency - mix.if_min - lo_min
                lo_f, if_f = divmod(relative_frequencies,
                                    adjusted_if_sweep_range)

                rsetattr(self, f"{pulse_type}_mix.frequency",
                         lo_min + adjusted_if_sweep_range * lo_f)
                rsetattr(self, f"{pulse_type}_pulse.frequency",
                         mix.if_min + if_f)

    @classmethod
    def from_dict(self, multi_tone_settings: Union[dict, MultiToneSettings]):
        if isinstance(multi_tone_settings, dict):
            return MultiToneSettings(**multi_tone_settings)
        elif isinstance(multi_tone_settings, Pulse):
            return multi_tone_settings
        else:
            raise ValueError("please provide PULSE SETTINGS in correct format")
