from __future__ import annotations

from dataclasses import dataclass
from typing import Union

import numpy as np
from eriks_stlab_toolbox.general.measurement import Mix, Pulse, Sweep
from eriks_stlab_toolbox.general.utilities import rsetattr


@dataclass
class TwoToneSettings:
    """dataclass for sweep settings for a two tone measurement
    Args:
        readout_pulse (Pulse): Pulse object, containing readout pulse data
        readout_mix (Mix): Mix object, containing data for readout pulse mixing
        drive_pulse (Pulse): Pulse object, containing drive pulse data
        drive_mix (Mix): Mix object, containing data for drive pulse mixing
        sweep (Sweep): Sweep object, containing general sweep variables
        --- see building_blocks.py for reference ---
    """

    readout_pulse: Union[Pulse, dict]
    readout_mix: Union[Mix, dict]
    drive_pulse: Union[Pulse, dict]
    drive_mix: Union[Mix, dict]
    sweep: Union[Sweep, dict]

    def __post_init__(self):
        self.readout_pulse = Pulse.from_dict(self.readout_pulse)
        self.drive_pulse = Pulse.from_dict(self.drive_pulse)
        self.readout_mix = Mix.from_dict(self.readout_mix)
        self.drive_mix = Mix.from_dict(self.drive_mix)
        self.sweep = Sweep.from_dict(self.sweep)

        # turn frequencies into seperate LO and IF frequencies

        for pulse_type in ["drive", "readout"]:
            mix = getattr(self, f"{pulse_type}_mix")
            pulse = getattr(self, f"{pulse_type}_pulse")

            # get number of if_sweep_range that fit in total f sweep range
            if_sweep_range = mix.if_max - mix.if_min
            n_lo_steps, remainder = divmod(
                (max(pulse.frequency) - min(pulse.frequency)),
                if_sweep_range,
            )

            # adjust if_sweep_range accoriding to total number fitting in it
            temp_adjusted_if_sweep_range = (max(pulse.frequency) - min(
                pulse.frequency)) / (n_lo_steps + (remainder > 0))

            lo_min = min(pulse.frequency) - mix.if_min
            if not (np.nan_to_num(temp_adjusted_if_sweep_range)):
                rsetattr(self, f"{pulse_type}_mix.frequency",
                         np.array([lo_min]))
                rsetattr(self, f"{pulse_type}_pulse.frequency",
                         np.array([mix.if_min]))

            else:
                # round if_sweep_range so that it is an integer multiple of f_step
                f_step = pulse.frequency[1] - pulse.frequency[0]

                points_per_if_sweep = int(
                    np.floor(temp_adjusted_if_sweep_range / f_step)) + 1
                adjusted_if_sweep_range = points_per_if_sweep * f_step

                relative_frequencies = pulse.frequency - mix.if_min - lo_min
                lo_f, if_f = divmod(relative_frequencies,
                                    adjusted_if_sweep_range)

                rsetattr(self, f"{pulse_type}_mix.frequency",
                         lo_min + adjusted_if_sweep_range * lo_f)
                rsetattr(self, f"{pulse_type}_pulse.frequency",
                         mix.if_min + if_f)

    @classmethod
    def from_dict(self, two_tone_settings_dict: Union[dict, TwoToneSettings]):
        if isinstance(two_tone_settings_dict, dict):
            return TwoToneSettings(**two_tone_settings_dict)
        elif isinstance(two_tone_settings_dict, Pulse):
            return two_tone_settings_dict
        else:
            raise ValueError("please provide PULSE SETTINGS in correct format")
