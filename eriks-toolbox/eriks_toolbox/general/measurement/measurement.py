from __future__ import annotations

import json
import os
from abc import ABC, abstractmethod
from datetime import datetime

from eriks_stlab_toolbox.general.measurement import Log


class Measurement(ABC):

    def __init__(self,
                 log_settings: Log,
                 output_path: str = os.path.dirname(__file__)):
        """abstract base class for a measurement classes
        Args:
            log_settings (dict): either Log object or dictionary for making Log object
            output_path (str): path to save data, metadata, script and settings in
        """

        self.now = datetime.now()
        self.log_settings = Log.from_dict(log_settings)

        self.output_path = output_path

        i = self.get_measurement_index()

        self.name = "__".join([
            self.log_settings.project,
            self.log_settings.log_name,
            f'{i:04}',
            self.now.strftime("%Y_%m_%d"),
            self.now.strftime("%H_%M_%S"),
        ])


    def get_measurement_index(self):

        i = len([
            folder_name for folder_name in os.listdir(self.output_path)
            if folder_name.startswith(
                f'{self.log_settings.project}__{self.log_settings.log_name}'
            )
        ])

        return i

    @classmethod
    def new(cls,
            log_settings: Log,
            *args,
            output_path: str = os.path.dirname(__file__),
            **kwargs) -> Measurement:
        """make new measurement object from input settings
        Args:
            log_settings (dict): either Log object or dictionary for making Log object
            *args: other args necessary to initiate child class
            output_path (str): path to save data, metadata, script and settings in
            **kwargs: other kwargs to initiate child class
        """

        measurement_object = cls(*args,
                                 log_settings=log_settings,
                                 output_path=output_path,
                                 **kwargs)

        measurement_object.settings = measurement_object.make_settings()
        measurement_object.tuned_settings = measurement_object.tune_settings()
        measurement_object.measurement_path = measurement_object.save_settings(
        )

        return measurement_object

    @classmethod
    def from_json(cls,
                  log_settings: Log,
                  path_to_json: str,
                  *args,
                  output_path: str = os.path.dirname(__file__),
                  **kwargs) -> Measurement:
        """make measurement object from json at path "path_to_json"
        Args:
            log_settings (dict): either Log object or dictionary for making Log object
            path_to_json (str): string where <old_settings>.json is saved
            *args: other args necessary to initiate child class
            output_path (str): path to save data, metadata, script and settings in
            **kwargs: other kwargs to initiate child class
        """

        measurement_object = cls(*args,
                                 log_settings=log_settings,
                                 output_path=output_path,
                                 **kwargs)

        with open(path_to_json) as old_settings_json:
            old_settings = json.load(old_settings_json)
        measurement_object.tuned_settings = old_settings
        measurement_object.measurement_path = measurement_object.save_settings(
        )

    @classmethod
    def from_dict(cls,
                  log_settings: Log,
                  settings_dict: str,
                  *args,
                  output_path: str = os.path.dirname(__file__),
                  **kwargs) -> Measurement:
        """make measurement object from settings in dict "settings_dict"

        Args:
            log_settings (dict): either Log object or dictionary for making Log object
            old_settings_path (str): string where <old_settings>.json is saved
            *args: other args necessary to initiate child class
            output_path (str): path to save data, metadata, script and settings in
            **kwargs: other kwargs to initiate child class
        """

        measurement_object = cls(*args,
                                 log_settings=log_settings,
                                 output_path=output_path,
                                 **kwargs)

        measurement_object.tuned_settings = settings_dict
        measurement_object.measurement_path = measurement_object.save_settings(
        )

        return measurement_object

    def do(self, save_data: bool = True, quick_plot: bool = True) -> None:
        """do the measurement, to be run after initiating using either .from_json, .from_dict or .new

        Args:
            save_data (bool: True): save data in outputpath/name/name.hdf5 + output_path/name/name.png
            quick_plot (bool: True): show plot of resulting data
        """

        # TODO: make labber compatible with these arguments
        self.measurement_program = self.make_measurement()
        self.measurement_result = self.do_measurement()
        return self.save_measurement(save_data, quick_plot)

    @abstractmethod
    def make_settings(self) -> dict:
        """gather default settings and prepare measurement settings
        Returns:
            default_settings (dict): dictionary containing default measurement settings
        """
        ...

    @abstractmethod
    def tune_settings(self) -> dict:
        """tune measurement settings specific variables (like frequency, power, etc.)
        Returns:
            tuned_settings (dict): dictionary containing specific measurement settings
        """
        ...

    @abstractmethod
    def save_settings(self) -> str:
        """save measurement settings in given output path
        Returns:
            output_path (str): full path at which measurement settings are saved
        """
        ...

    @abstractmethod
    def make_measurement(self):
        """load measurement settings and create a measurement object"""
        ...

    @abstractmethod
    def do_measurement(self):
        """execute measurement program"""
        ...

    @abstractmethod
    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        """save the measured data as hdf5 in xarray format in default or specified self.output_path
        Args:
            save_data (bool: True): save data in outputpath/name/name.hdf5 + output_path/name/name.png
            quick_plot (bool: True): show plot of resulting data
        """
        ...
