# -*- coding: utf-8 -*-
# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"></ul></div>

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"></ul></div>

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"></ul></div>

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"></ul></div>

from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np
from dexplore import (interactive_colormap, interactive_linecut,
                      interactive_linecut_and_colormap, load_dat_meta_txt)
from eriks_stlab_toolbox.general.utilities import (HiddenPrints, Residuals,
                                                   bool_to_sign)
from lmfit import Parameter, Parameters, minimize, report_fit
from scipy.signal import savgol_filter as smooth
from stlabutils import (S11back, background2min, getwidth_phase,
                        readdata, trim, un_realimag, realimag)

print("Hello n3rd$!")


def quick_plot_data(
        file_name,
        style="lcm"):  # lc: linecut, cm: colormap, lcm: linecut + colormap
    function_dict = {
        "lc": interactive_colormap,
        "cm": interactive_linecut,
        "lcm": interactive_linecut_and_colormap,
    }

    function_dict[style](load_dat_meta_txt(file_name))


# +
def S11residual(params, frec, data, ftype=None):

    model = S11func(frec, params, ftype)
    residual = model - data
    return realimag(residual)

def S11func(frec, params, ftype=None):
    """
    Function for total response from background model and resonator response
    """
    
    model = S11theo(frec, params, ftype) * S11back(frec, params)
    return model

def S11theo(frec, params, ftype=None):
    Qint = params['Qint'].value
    Qext = params['Qext'].value
    f0 = params['f0'].value
    w0 = f0 * 2 * np.pi
    w = 2 * np.pi * frec
    kint = w0 / Qint
    kext = w0 / Qext
    dw = w - w0

#     theta = params['theta']

    if ftype == '-B':
    
        return (kint + 2j * dw) / (kext + kint + 2j * dw)
    
    elif ftype == '-A':
    
        return (- kext + kint + 2j * dw) / (kext + kint + 2j * dw)
    
#     return ((2. * kint + 4j * dw) * np.exp(1j * theta)) / (2 * kext + 3 * kint + 6j * dw)
    


# +
class FanoFitter:

    def __init__(self, data, settings):
        # self.file_name = file_name
        # with HiddenPrints():
        
        self.data = data
        self.settings = settings

    def single_fit(self, re_label, im_label, f_label, col=0, show_prints=False):
        """wrapper that runs self.fit once"""

        frequencies, scattering = self.get_data(re_label, im_label, f_label, col)

        if show_prints:
            fit, frequencies, scattering = self.fit_wrapper(
                frequencies, scattering)
        else:
            with HiddenPrints():
                fit, frequencies, scattering = self.fit_wrapper(
                    frequencies, scattering)

        fitted_scattering = scattering + un_realimag(fit.residual)
        residuals = Residuals(np.abs(scattering), np.abs(fitted_scattering),
                              len(fit.params))

        self.fano_plot(
            frequencies,
            scattering,
            fit=fit,
            # title=self.file_name.split("/")[-1],
        )
#         plt.show()

        return fit, residuals

    def double_fit(self, re_label, im_label, f_label, col=0, show_prints=False):
        """wrarpper that runs self.fit twice"""

        frequencies, scattering = self.get_data(re_label, im_label, f_label, col)

        if not isinstance(self.settings, tuple):
            settings_1 = settings_2 = self.settings
        else:
            settings_1, settings_2 = self.settings

        self.settings = settings_1
        if show_prints:
            fit_0, frequencies, scattering = self.fit_wrapper(
                frequencies, scattering)
        else:
            with HiddenPrints():
                fit_0, frequencies, scattering = self.fit_wrapper(
                    frequencies, scattering)

        fitted_scattering_0 = scattering + un_realimag(fit_0.residual)
        residuals_0 = Residuals(np.abs(scattering),
                                np.abs(fitted_scattering_0), len(fit_0.params))

        self.fano_plot(
            frequencies,
            scattering,
            fit=fit_0,
            # title="\n\n".join(
            #     [f'{self.file_name.split("/")[-1]}', "first fit"]),
        )

        self.settings = settings_2
        self.settings.oldpars = fit_0.params
        if show_prints:
            fit_1, frequencies, scattering = self.fit_wrapper(
                frequencies, scattering)
        else:
            with HiddenPrints():
                fit_1, frequencies, scattering = self.fit_wrapper(
                    frequencies, scattering)

        fitted_scattering_1 = scattering + un_realimag(fit_1.residual)
        residuals_1 = Residuals(np.abs(scattering),
                                np.abs(fitted_scattering_1), len(fit_1.params))

        self.fano_plot(
            frequencies,
            scattering,
            fit=fit_1,
            title="second fit",
        )
        plt.show()

        self.settings = (settings_1, settings_2)

        return fit_0, fit_1, residuals_0, residuals_1

    def get_data(self, re_label, im_label, f_label, col):
        frequencies = np.array(self.data[f_label].values)
        scattering = np.array(self.data[re_label][col].values +
                              1j * self.data[im_label][col].values)

        return frequencies, scattering

    def fit_wrapper(self, frequencies, scattering):
        """this wrapper runs all functions needed for nice fitting"""

        s = self.settings

        smoothed_scattering, sdm_smoothed_arg = self.smoothen(scattering)
        resonance_index, index_min, index_max = self.find_peak(
            sdm_smoothed_arg, frequencies)

        d_index = index_max - index_min

        print("\n".join([
            f"Peak limits: min: {index_min}, max: {index_max}",
            f"Lower edge: {frequencies[index_min]}",
            f"Center:  {frequencies[resonance_index]}",
            f"Upper edge: {frequencies[index_max]}",
            f"Points in width: {d_index}",
        ]))

        if s.fitwidth:
            frequencies, scattering, resonance_index, index_min, index_max = self.crop_peak(
                frequencies,
                scattering,
                resonance_index,
                index_min,
                index_max,
            )

        # __ trim peak from data (trimwidth times the width) __
        background_frequencies, background_noise = trim(
            frequencies,
            scattering,
            max(round(resonance_index - s.trimwidth * d_index), 0),
            min(round(resonance_index + s.trimwidth * d_index), len(frequencies)),
        )

        parameters_0 = self.initialize_background(
            frequencies,
            background_frequencies,
            background_noise,
            smoothed_scattering,
        )

        background_fit, corrected_scattering = self.fit_background(
            frequencies,
            scattering,
            parameters_0,
            background_frequencies,
            background_noise,
        )

        parameters_1 = self.initialize_fano(
            frequencies,
            corrected_scattering,
            resonance_index,
            index_min,
            index_max,
        )

        fano_fit = self.fit_fano(frequencies, scattering, parameters_1,
                                 background_fit)

        if s.fitbackground:
            final_fit = self.fit_all(
                frequencies,
                scattering,
                fano_fit.params,
            )
        else:
            final_fit = fano_fit

        if s.doplots:
            fig_0 = self.fano_plot(frequencies, scattering, title="ORIGINAL")

            for ax in fig_0.get_axes()[:-1]:
                ax.axvline(frequencies[resonance_index], color="r")
                if s.trimwidth:
                    ax.axvline(frequencies[index_min], color="r")
                    ax.axvline(frequencies[index_max], color="r")

            self.fano_plot(background_frequencies,
                           background_noise,
                           fit=background_fit,
                           title="BACKGROUND")
            self.fano_plot(frequencies,
                           corrected_scattering,
                           title="CORRECTED")
            self.fano_plot(frequencies,
                           scattering,
                           fit=fano_fit,
                           title="PRE-FINAL FIT")
            self.fano_plot(frequencies,
                           scattering,
                           fit=final_fit,
                           title="FINAL FIT")

        return final_fit, frequencies, scattering

    def smoothen(self, scattering):
        """smooth data for initial guesses
        Arg:
            data
        Returns:
            smoothed_data
            squared_deviation_from_mean_smoothed_arg
        """

        margin = self.settings.margin

        if not margin:  # if no smooting desired, pass None as margin
            margin = 0
            smoothed_re = scattering.real
            smoothed_im = scattering.imag

        elif (not isinstance(margin, int)) or margin % 2 == 0 or margin <= 3:
            raise ValueError(
                "margin has to be either None, 0, or an odd integer larger than 3"
            )
        else:
            smoothed_re = np.array(smooth(scattering.real, margin, 3))
            smoothed_im = np.array(smooth(scattering.imag, margin, 3))
        smoothed_scattering = np.array(
            [x + 1j * y for x, y in zip(smoothed_re, smoothed_im)])

        # make smoothed phase vector: remove 2pi jumps
        smoothed_arg = np.unwrap(np.angle(smoothed_scattering))

        # get arg differences and their 'squared deviation from mean'
        d_smoothed_arg = np.diff(smoothed_arg)
        sdm_smoothed_arg = [
            np.power(x - np.average(d_smoothed_arg), 2.0)
            for x in d_smoothed_arg
        ]

        return smoothed_scattering, sdm_smoothed_arg

    def find_peak(self, sdm_smoothed_arg, frequencies=None):
        margin = self.settings.margin

        # __ get resonance peak width __
        resonance_index = np.argmax(
            sdm_smoothed_arg[margin:-(margin + 1)]) + margin
        if frequencies is not None and self.settings.reusefitpars and self.settings.oldpars:
            parameters = self.settings.oldpars
            try:
                qtot = (parameters["Qint"].value**-1 +
                        parameters["Qext"].value**-1)**-1
            except Exception:
                try:
                    qtot = parameters["Qint"].value
                except Exception:
                    qtot = parameters["Qext"].value

            ktot = parameters["f0"].value / qtot
            f_per_index = (frequencies[-1] -
                           frequencies[0]) / (len(frequencies) - 1)
            index_per_ktot = ktot / f_per_index
            print("HERE")
            print(ktot)
            print(f_per_index)
            print(index_per_ktot)

            index_min = int(resonance_index - index_per_ktot / 2)
            index_max = round(resonance_index + index_per_ktot / 2)

            print(index_min, index_max)

        else:
            index_min, index_max = getwidth_phase(resonance_index,
                                                  sdm_smoothed_arg, margin)

        return resonance_index, index_min, index_max

    def crop_peak(self, frequencies, scattering, resonance_index, index_min,
                  index_max):
        """crop out peak if fitwidth is given"""

        fitwidth = self.settings.fitwidth
        d_index = index_max - index_min

        i1 = max(round(resonance_index - d_index * fitwidth), 0)
        i2 = min(round(resonance_index + d_index * fitwidth), len(frequencies))

        frequencies = frequencies[i1:i2]
        scattering = scattering[i1:i2]
        resonance_index -= i1
        index_min -= i1
        index_max -= i1

        return frequencies, scattering, resonance_index, index_min, index_max

    def initialize_background(self, frequencies, background_frequencies,
                              background_noise, smoothed_scattering):
        """create initial parameters for background fit"""

        reusebackpars = self.settings.reusebackpars
        oldpars = self.settings.oldpars

        # __ initialize parameters __
        parameters = {"a": 0, "b": 0, "c": 0, "ap": 0, "bp": 0, "cp": 0}

        # make initial background guesses
        if not reusebackpars or oldpars is None:
            parameters["b"] = (np.abs(smoothed_scattering)[-1] -
                               np.abs(smoothed_scattering)[0]) / (
                                   frequencies[-1] - frequencies[0])
            parameters["a"] = np.average(
                np.abs(smoothed_scattering
                       )) - parameters["b"] * background_frequencies[0]

            d_frequency = np.diff(background_frequencies)
            d_theta = np.diff(np.angle(background_noise))
            d_th_d_f = d_theta / d_frequency
            d_th_d_f = d_th_d_f[(np.abs(d_theta) <= np.pi)
                                & np.isfinite(d_th_d_f)]

            parameters["bp"] = np.average(d_th_d_f)
            parameters["ap"] = np.unwrap(
                np.angle(background_noise
                         ))[0] - parameters["bp"] * background_frequencies[0]
        else:
            parameters = {key: oldpars[key].value for key in parameters}

        return parameters

    def fit_background(self, frequencies, scattering, initial_parameters,
                       background_frequencies, background_noise):
        ftype = self.settings.ftype
        fitbackground = self.settings.fitbackground

        parameters = Parameters()

        for key, value in initial_parameters.items():
            parameters.add(key,
                           value=value,
                           vary=(fitbackground and (key not in ["cp"])))
            
#         parameters['a'].value = bool_to_sign(ftype in ['-A', '-B', 'X'])

        # __ fit background and remove from original data __
        background_fit = minimize(background2min,
                                  parameters,
                                  args=(background_frequencies,
                                        background_noise))

        report_fit(background_fit.params)

        fitted_background_noise = np.array(
            [S11back(f, background_fit.params) for f in frequencies])

        corrected_scattering = -scattering / fitted_background_noise * bool_to_sign(
            ftype in ["A", "B", "X"])

        return background_fit, corrected_scattering

    def initialize_fano(self, frequencies, corrected_scattering,
                        resonance_index, index_min, index_max):
        ftype = self.settings.ftype
        reusefitpars = self.settings.reusefitpars
        oldpars = self.settings.oldpars

        parameters = {"Qint": 0, "Qext": 0, "f0": 0}
#                       , "theta": 0}

        resonance_frequency = frequencies[resonance_index]

        if (not reusefitpars) | (oldpars is None):
            ktot = np.abs(frequencies[index_max] - frequencies[index_min])
            if "A" in ftype:
                Tres = np.abs(1 + bool_to_sign(ftype == "A") *
                              corrected_scattering[resonance_index])
                kext0 = ktot * Tres / 2.0
            else:
                Tres = np.abs(corrected_scattering[resonance_index])
                kext0 = (1 + bool_to_sign(ftype == "B") * Tres) * ktot

            kint0 = ktot - kext0
            if kint0 <= 0.0:
                kint0 = kext0

            parameters["Qint"] = resonance_frequency / kint0
            parameters["Qext"] = resonance_frequency / kext0
            parameters["f0"] = resonance_frequency

        else:
            parameters = {key: oldpars[key].value for key in parameters}

        return parameters

    def fit_fano(self, frequencies, scattering, initial_parameters,
                 background_fit):
        ftype = self.settings.ftype

        parameters = background_fit.params
        for p in parameters.values():
            p.vary = False

        for key, value in initial_parameters.items():
            parameters.add(
                key,
                value=value,
                vary=True,
                min=(0.0 if "Q" in key else -np.inf),
#                 max=( if 'Q' in key else np.inf),
                # max=10e20,
            )

        # __ do (pre) final fit __
        fano_fit = minimize(S11residual,
                            parameters,
                            args=(frequencies, scattering, ftype))

        report_fit(fano_fit.params)
        parameters = fano_fit.params

        return fano_fit

    def fit_all(self, frequencies, scattering, parameters):
        ftype = self.settings.ftype

        # __ do final fit (varying all params) __

        for name, p in parameters.items():
            p.vary = name not in ["cp"]

        final_fit = minimize(S11residual,
                             parameters,
                             args=(frequencies, scattering, ftype))

        report_fit(final_fit.params)
        parameters = final_fit.params

        return final_fit

    @staticmethod
    def fano_plot(frequencies, scattering, fit=None, title="", wrap=False):
        plt.rcParams.update({
            "font.size": 25,
            "axes.formatter.useoffset": False,
            "axes.formatter.use_mathtext": True,
            "lines.linewidth": 5,
            "xtick.major.size": 10,
            "xtick.major.width": 2,
            "ytick.major.size": 10,
            "ytick.major.width": 2,
        })

        fig = plt.figure(figsize=(40, 10))
        fig.suptitle(title)

        ax1 = fig.add_subplot(1, 4, 1)
        ax1.set_title("Abs")
        ax1.plot(frequencies, np.abs(scattering))
        ax1.ticklabel_format(useOffset=True)

        ax2 = fig.add_subplot(1, 4, 2)
        ax2.set_title("Re / Im")
        ax2.plot(frequencies, scattering.real)
        ax2.plot(frequencies, scattering.imag)
        ax2.ticklabel_format(useOffset=True)

        ax3 = fig.add_subplot(1, 4, 3)
        ax3.set_title("Phase")
        ax3.plot(frequencies, np.unwrap(np.angle(scattering), discont=-np.pi))
        ax3.ticklabel_format(useOffset=True)

        ax4 = fig.add_subplot(1, 4, 4)
        ax4.set_title("Polar")
        ax4.plot(scattering.real, scattering.imag)
        ax4.set_aspect("equal", "datalim")

        if fit is not None:
            complex_residual = un_realimag(fit.residual)
            fit = scattering + complex_residual
            ax1.plot(frequencies, np.abs(fit))
            ax2.plot(frequencies, fit.real)
            ax2.plot(frequencies, fit.imag)
            ax3.plot(frequencies, np.unwrap(np.angle(fit)))
            ax4.plot(fit.real, fit.imag)

        plt.tight_layout()

        return fig


# -

@dataclass
class FanoFitSettings:
    r"""This function was based on the 'S11fit' from stlabutils.S11fit:

    **MAIN FIT ROUTINE**

    Fits complex data S11 vs frequency to one of 4 models adjusting for a
    multiplicative complex background. It fits the data in three steps.
    Firstly it fits the background signal removing a certain window around
    the detected peak position. Then it fits the model times the background
    to the full data set keeping the background parameters fixed at the
    fitted values. Finally it refits all background and model parameters
    once more starting from the previously fitted values. The fit model is:

    .. math:: \Gamma'(\omega)=(a+b\omega+c\omega^2)\exp(j(a'+b'\omega))\cdot
        \Gamma(\omega,Q_\\textrm{int},Q_\\textrm{ext},\\theta),


    Parameters
    ----------
    frequencies : array_like
        Array of X values (typically frequency)
    s21 : array_like
        Complex array of Z values (typically S11 data)
    ftype : {'A','B','-A','-B', 'X'}, optional
        Fit model function (A,B,-A,-B, see S11theo for formulas)
    fitbackground : bool, optional
        If "True" will attempt to fit and remove background.
        If "False", will use a constant background equal to 1
        and fit only model function to data.
    trimwidth : float, optional
        Number of linewidths around resonance (estimated pre-fit) to remove for
        background only fit.
    doplots : bool, optional
        If "True", shows debugging and intermediate plots
    margin : float, optional
        Smoothing window to apply to signal for initial guess procedures
        (the fit uses unsmoothed data)
    oldpars : lmfit.Parameters, optional
        Parameter data from previous fit (expects lmfit Parameter object).
        Used when "reusebackpars" is "True" or "reusefitpars" is "True".
    reusebackpars : bool, optional
        If set to True, uses parameters provided in "oldpars" as initial guess
        for fit parameters in background model fit (ignored by background fit)
    reusefitpars : bool, optional
        If set to True, uses parameters provided in "oldpars" as initial guess
        for fit parameters in main model fit (ignored by background fit)
    fitwidth : float, optional
        If set to a numerical value, will trim the signal to a certain number
        of widths around the resonance for all the fit

    Returns
    -------
    params : lmfit.Parameters
        Fitted parameter values
    freq : numpy.ndarray
        Array of frequency values within the fitted range
    S11: numpy.ndarray
        Array of complex signal values within the fitted range
    finalresult : lmfit.MinimizerResult
        The full minimizer result object (see lmfit documentation for details)

    """

    ftype: float = "-B"
    margin: int = 51
    trimwidth: float = 20
    fitwidth: float = None
    oldpars: Parameters = None
    fitbackground: bool = True
    reusebackpars: bool = False
    reusefitpars: bool = False
    doplots: bool = False


@dataclass(init=False)
class FanoFitParameters:
    """dataclass container for s11 fit parameters"""

    f0: float
    Qint: float
    Qext: float

    def __init__(self, **kwargs):
        items = [(k, v) for k, v in kwargs.items()
                 if k in self.__dataclass_fields__]
        for key, value in items:
            if value.stderr is None:
                value.stderr = 0

            setattr(self, key, value)

    def __repr__(self):
        return "\n".join([
            f"f0: {self.f0.value/1e9:.6f} ± {self.f0.stderr/1e9:.6f} GHz",
            f"Qi: {self.Qi.value:.4e} ± {self.Qi.stderr:.4e}",
            f"Qe: {self.Qe.value:.4e} ± {self.Qe.stderr:.4e}",
            f"Qtot: {self.Qtot.value:.4e} ± {self.Qtot.stderr:.4e}",
            f"ki: {self.ki.value/1e3:.4f} ± {self.ki.stderr/1e3:.4f} kHz",
            f"ke: {self.ke.value/1e3:.4f} ± {self.ke.stderr/1e3:.4f} kHz",
            f"ktot: {self.ktot.value/1e3:.4f} ± {self.ktot.stderr/1e3:.4f} kHz",
        ])

    @property
    def Qtot(self):
        value = self.f0 / self.ktot
        error = value * np.sqrt((self.f0.stderr / self.f0.value)**2 +
                                (self.ktot.stderr / self.ktot.value)**2)
        qtot = Parameter(name="Qtot", value=value)
        qtot.stderr = error
        return qtot

    @property
    def ki(self):
        value = self.f0 / self.Qi
        error = value * np.sqrt((self.f0.stderr / self.f0.value)**2 +
                                (self.Qi.stderr / self.Qi.value)**2)
        ki = Parameter(name="ki", value=value)
        ki.stderr = error
        return ki

    @property
    def ke(self):
        value = self.f0 / self.Qe
        error = value * np.sqrt((self.f0.stderr / self.f0.value)**2 +
                                (self.Qe.stderr / self.Qe.value)**2)
        ke = Parameter(name="ki", value=value)
        ke.stderr = error
        return ke

    @property
    def ktot(self):
        value = self.ki + self.ke
        error = np.sqrt(self.ki.stderr**2 + self.ke.stderr**2)
        ktot = Parameter(name="ktot", value=value)
        ktot.stderr = error
        return ktot

    @property
    def Qi(self):
        return self.Qint

    @property
    def Qe(self):
        return self.Qext
