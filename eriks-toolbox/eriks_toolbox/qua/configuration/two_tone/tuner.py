import numpy as np
from eriks_stlab_toolbox.general.utilities import listify, replace_strings
from qualang_tools.config.integration_weights_tools import \
    convert_integration_weights
from qualang_tools.units import unit
from scipy.signal import gaussian

unit_tools = unit(coerce_to_integer=True)


class TwoToneTuner:

    @staticmethod
    def set_drive_if_frequency(object_to_tune, value):
        value *= unit_tools.Hz

        old_config = object_to_tune.get_settings("config")
        object_to_tune.set_settings(
            "config",
            replace_strings(old_config, "<drive_if_frequency>",
                            np.mean(value)),
        )

        object_to_tune.set_settings(
            "sweep.drive_if_frequency",
            listify(value),
        )
        return object_to_tune

    @staticmethod
    def set_drive_lo_frequency(object_to_tune, value):
        value *= unit_tools.Hz

        old_config = object_to_tune.get_settings("config")
        object_to_tune.set_settings(
            "config",
            replace_strings(old_config, "<drive_lo_frequency>",
                            np.mean(value)),
        )

        object_to_tune.set_settings(
            "sweep.drive_lo_frequency",
            listify(value),
        )
        return object_to_tune

    @staticmethod
    def set_drive_if_power(object_to_tune, value):
        amplitude = object_to_tune.get_settings(
            "config.waveforms.saturation_wf.sample")

        value *= unit_tools.V / amplitude

        while max(value) > 2:
            value = [v / 2 for v in value]
            amplitude *= 2

        object_to_tune.set_settings(
            "config.waveforms.saturation_wf.sample",
            amplitude,
        )

        object_to_tune.set_settings(
            "config.waveforms.pi_wf.samples",
            amplitude,
        )

        object_to_tune.set_settings(
            "sweep.drive_if_power",
            listify(value),
        )
        return object_to_tune

    @staticmethod
    def set_drive_pulse_width(object_to_tune, value):
        value = [int(v * unit_tools.s) for v in value]

        old_config = object_to_tune.get_settings("config")
        object_to_tune.set_settings(
            "config",
            replace_strings(old_config, "<drive_pulse_width>", min(value)),
        )

        old_sample = object_to_tune.get_settings(
            "config.waveforms.pi_wf.samples")
        object_to_tune.set_settings(
            "config.waveforms.pi_wf.samples",
            (old_sample * gaussian(min(value),
                                   min(value) / 5)).tolist(),
        )

        object_to_tune.set_settings(
            "sweep.drive_pulse_width",
            listify(value),
        )

        return object_to_tune

    @staticmethod
    def set_drive_pulse_shape(object_to_tune, value):
        object_to_tune.set_settings(
            "sweep.drive_pulse_shape",
            value,
        )
        return object_to_tune

    @staticmethod
    def set_readout_if_frequency(object_to_tune, value):
        value *= unit_tools.Hz

        old_config = object_to_tune.get_settings("config")
        object_to_tune.set_settings(
            "config",
            replace_strings(old_config, "<readout_if_frequency>",
                            np.mean(value)),
        )

        object_to_tune.set_settings(
            "sweep.readout_if_frequency",
            listify(value),
        )
        return object_to_tune

    @staticmethod
    def set_readout_lo_frequency(object_to_tune, value):
        value *= unit_tools.Hz

        old_config = object_to_tune.get_settings("config")
        object_to_tune.set_settings(
            "config",
            replace_strings(old_config, "<readout_lo_frequency>",
                            np.mean(value)),
        )

        object_to_tune.set_settings(
            "sweep.readout_lo_frequency",
            listify(value),
        )
        return object_to_tune

    @staticmethod
    def set_readout_if_power(object_to_tune, value):
        amplitude = object_to_tune.get_settings(
            "config.waveforms.ro_wf.sample")

        value *= unit_tools.V / amplitude

        while max(value) > 2:
            value = [v / 2 for v in value]
            amplitude *= 2

        object_to_tune.set_settings(
            "config.waveforms.ro_wf.sample",
            amplitude,
        )

        object_to_tune.set_settings(
            "sweep.readout_if_power",
            listify(value),
        )
        return object_to_tune

    @staticmethod
    def set_readout_angle(object_to_tune, value):

        value = listify(value)

        object_to_tune.set_settings(
            "config",
            replace_strings(
                object_to_tune.get_settings("config"),
                "<cosine_angle>",
                np.cos(value[0]),
            ),
        )
        object_to_tune.set_settings(
            "config",
            replace_strings(
                object_to_tune.get_settings("config"),
                "<sine_angle>",
                np.sin(value[0]),
            ),
        )
        object_to_tune.set_settings(
            "config",
            replace_strings(
                object_to_tune.get_settings("config"),
                "<minus_cosine_angle>",
                -np.cos(value[0]),
            ),
        )
        object_to_tune.set_settings(
            "config",
            replace_strings(
                object_to_tune.get_settings("config"),
                "<minus_sine_angle>",
                -np.sin(value[0]),
            ),
        )

        object_to_tune.set_settings(
            "sweep.readout_angle",
            value,
        )

        return object_to_tune

    @staticmethod
    def set_readout_integration_weights(object_to_tune, value):
        integration_weights = np.load(value)

        object_to_tune.set_settings(
            "config.integration_weights",
            {
                "cosine": {
                    "cosine":
                    convert_integration_weights(integration_weights["cosine"]),
                    "sine":
                    convert_integration_weights(integration_weights["sine"]),
                },
                "sine": {
                    "cosine":
                    convert_integration_weights(
                        integration_weights["minus_sine"]),
                    "sine":
                    convert_integration_weights(integration_weights["cosine"]),
                },
                "minus_sine": {
                    "cosine":
                    convert_integration_weights(integration_weights["sine"]),
                    "sine":
                    convert_integration_weights(
                        integration_weights["minus_cosine"]),
                },
            },
        )
        return object_to_tune

    @staticmethod
    def set_readout_pulse_width(object_to_tune, value):
        value = [int(v * unit_tools.s) for v in value]

        old_config = object_to_tune.get_settings("config")
        object_to_tune.set_settings(
            "config",
            replace_strings(old_config, "<readout_pulse_width>", max(value)),
        )

        object_to_tune.set_settings(
            "sweep.readout_pulse_width",
            listify(value),
        )
        return object_to_tune

    @staticmethod
    def set_idle_time(object_to_tune, value):
        value = [int(v * unit_tools.s) for v in value]

        object_to_tune.set_settings(
            "sweep.idle_time",
            listify(value),
        )
        return object_to_tune

    @staticmethod
    def set_n_single_shots(object_to_tune, value):
        object_to_tune.set_settings(
            "sweep.n_single_shots",
            int(value),
        )
        return object_to_tune

    @staticmethod
    def set_n_averages(object_to_tune, value):
        object_to_tune.set_settings(
            "sweep.n_averages",
            int(value),
        )
        return object_to_tune

    @staticmethod
    def set_overlapping_readout(object_to_tune, value):
        object_to_tune.set_settings(
            "sweep.overlapping_readout",
            value,
        )
        return object_to_tune

    @staticmethod
    def set_time_of_flight(object_to_tune, value):
        value = round(value * unit_tools.s)

        old_config = object_to_tune.get_settings("config")
        object_to_tune.set_settings(
            "config", replace_strings(old_config, "<time_of_flight>", value))

        object_to_tune.set_settings(
            "sweep.time_of_flight",
            value,
        )
        return object_to_tune

    @staticmethod
    def set_wait_between(object_to_tune, value):
        value = round(value * unit_tools.s)

        object_to_tune.set_settings(
            "sweep.wait_between",
            value,
        )

        return object_to_tune


if __name__ == "__main__":
    pass
