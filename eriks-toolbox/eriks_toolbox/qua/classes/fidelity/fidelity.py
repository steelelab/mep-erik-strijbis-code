# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"></ul></div>

import os
from copy import deepcopy
from dataclasses import asdict

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from eriks_stlab_toolbox.general.measurement import (MultiToneSettings,
                                                     TwoToneSettings)
from eriks_stlab_toolbox.general.utilities.functions import rgetattr
from eriks_stlab_toolbox.qua.classes.measurement import QuaMeasurement
from eriks_stlab_toolbox.qua.configuration import MultiToneTuner, TwoToneTuner
from qm.qua import (align, amp, declare, declare_stream, dual_demod, fixed,
                    for_, measure, play, program, save, stream_processing,
                    wait)
from qualang_tools.analysis.discriminator import two_state_discriminator
from qualang_tools.units import unit

unit_tools = unit(coerce_to_integer=True)


class Fidelity(QuaMeasurement):
    measurement_type = "two_tone"

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = TwoToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        tuned_object = TwoToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = TwoToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = TwoToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = TwoToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        if integration_weights := self.sweep_settings.readout_pulse.path_to_integration_weights:
            tuned_object = TwoToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = TwoToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        tuned_object = TwoToneTuner.set_drive_if_frequency(
            tuned_object, self.sweep_settings.drive_pulse.frequency)
        tuned_object = TwoToneTuner.set_drive_lo_frequency(
            tuned_object, self.sweep_settings.drive_mix.frequency)
        tuned_object = TwoToneTuner.set_drive_if_power(
            tuned_object, self.sweep_settings.drive_pulse.power)
        tuned_object = TwoToneTuner.set_drive_pulse_width(
            tuned_object, self.sweep_settings.drive_pulse.width)
        tuned_object = TwoToneTuner.set_drive_pulse_shape(
            tuned_object, self.sweep_settings.drive_pulse.shape)

        tuned_object = TwoToneTuner.set_n_single_shots(
            tuned_object, self.sweep_settings.sweep.n_single_shots)
        tuned_object = TwoToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = TwoToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = TwoToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        DRIVE_IF_POWER = self.get_settings("sweep.drive_if_power")[0]
        DRIVE_PULSE_SHAPE = self.get_settings("sweep.drive_pulse_shape")
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]

        N_SINGLE_SHOTS = self.get_settings("sweep.n_single_shots")
        N_AVERAGES = self.get_settings("sweep.n_averages")
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        with program() as ro_int_angle_opt:
            m = declare(int)  # QUA variable for the averaging loop
            n = declare(int)  # QUA variable for the averaging loop

            Ig = declare(
                fixed
            )  # QUA variable for the 'I' quadrature when the qubit is in |g>
            Qg = declare(
                fixed
            )  # QUA variable for the 'Q' quadrature when the qubit is in |g>
            Ig_st = declare_stream()
            Qg_st = declare_stream()

            Ie = declare(
                fixed
            )  # QUA variable for the 'I' quadrature when the qubit is in |e>
            Qe = declare(
                fixed
            )  # QUA variable for the 'Q' quadrature when the qubit is in |e>
            Ie_st = declare_stream()
            Qe_st = declare_stream()

            n_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):
                with for_(m, 0, m < N_SINGLE_SHOTS, m + 1):

                    # Measure the ground state.
                    # Measure the state of the resonator
                    measure(
                        amp(READOUT_IF_POWER) * "readout",
                        "resonator",
                        None,
                        dual_demod.full("cosine", "out1", "sine", "out2", Ig),
                        dual_demod.full("minus_sine", "out1", "cosine", "out2",
                                        Qg),
                    )
                    # Wait for the qubit to decay to the ground state in the case of measurement induced transitions
                    wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                    # Save the 'I' & 'Q' quadratures to their respective streams for the ground state
                    save(Ig, Ig_st)
                    save(Qg, Qg_st)

                    align()  # global align
                    # Play the x180 gate to put the qubit in the excited state
                    play(amp(DRIVE_IF_POWER) * DRIVE_PULSE_SHAPE,
                         "qubit",
                         duration=DRIVE_PULSE_WIDTH * unit_tools.ns)
                    # Align the two elements to measure after playing the qubit pulse.
                    align("qubit", "resonator")
                    # Measure the state of the resonator
                    measure(
                        amp(READOUT_IF_POWER) * "readout",
                        "resonator",
                        None,
                        dual_demod.full("cosine", "out1", "sine", "out2", Ie),
                        dual_demod.full("minus_sine", "out1", "cosine", "out2",
                                        Qe),
                    )
                    # Wait for the qubit to decay to the ground state
                    wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                    # Save the 'I' & 'Q' quadratures to their respective streams for the excited state
                    save(Ie, Ie_st)
                    save(Qe, Qe_st)

                save(n, n_st)

            with stream_processing():
                n_st.save("iteration")
                # mean values
                Ig_st.buffer(N_SINGLE_SHOTS).average().save("Ig")
                Qg_st.buffer(N_SINGLE_SHOTS).average().save("Qg")
                Ie_st.buffer(N_SINGLE_SHOTS).average().save("Ie")
                Qe_st.buffer(N_SINGLE_SHOTS).average().save("Qe")

        return ro_int_angle_opt

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):

        DRIVE_FREQUENCY = self.get_settings("sweep.drive_if_frequency")[
            0] + self.get_settings("sweep.drive_lo_frequency")[0]
        DRIVE_IF_POWER = self.get_settings("sweep.drive_if_power")[0]
        DRIVE_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.pi_wf.samples"))
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_FREQUENCY = self.get_settings("sweep.readout_if_frequency")[
            0] + self.get_settings("sweep.readout_lo_frequency")[0]
        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]
        READOUT_IF_POWER_SCALAR = self.get_settings(
            "config.waveforms.ro_wf.sample")

        READOUT_PULSE_WIDTH = self.get_settings("sweep.readout_pulse_width")[0]
        READOUT_ANGLE = self.get_settings("sweep.readout_angle")
        if not READOUT_ANGLE:
            READOUT_ANGLE = 0
        else:
            READOUT_ANGLE = READOUT_ANGLE[0]

        N_AVERAGES = self.get_settings("sweep.n_averages")
        N_SINGLE_SHOTS = self.get_settings("sweep.n_single_shots")

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        Ig = unit_tools.demod2volts(
            res_handles.get("Ig").fetch_all(),
            READOUT_PULSE_WIDTH)  # transpose to get amplitude over axis 0
        Qg = unit_tools.demod2volts(
            res_handles.get("Qg").fetch_all(), READOUT_PULSE_WIDTH)
        Ie = unit_tools.demod2volts(
            res_handles.get("Ie").fetch_all(), READOUT_PULSE_WIDTH)
        Qe = unit_tools.demod2volts(
            res_handles.get("Qe").fetch_all(), READOUT_PULSE_WIDTH)

        readout_powers_in_v = READOUT_IF_POWER * READOUT_IF_POWER_SCALAR
        drive_powers_in_v = DRIVE_IF_POWER * round(DRIVE_IF_POWER_SCALAR, 3)

        signal = Ie - Ig
        signal_without_outliers = signal[np.abs(signal -
                                                np.average(signal)) < 2 *
                                         np.std(signal)]
        snr = np.average(signal_without_outliers**
                         2) / np.var(signal_without_outliers)

        angle, threshold, fidelity, gg, ge, eg, ee = two_state_discriminator(
            Ig, Qg, Ie, Qe, b_print=True, b_plot=False)

        dims = [
            "single_shot_index",
        ]

        dataset = xr.Dataset(
            data_vars={
                "Ig (V)": (dims, Ig),
                "Qg (V)": (dims, Qg),
                "Ie (V)": (dims, Ie),
                "Qe (V)": (dims, Qe),
            },
            coords={
                "single_shot_index":
                (("single_shot_index", np.arange(N_SINGLE_SHOTS)))
            },
            attrs={
                "angle [rad]": angle,
                "threshold [V]": threshold,
                "fidelity [%]": fidelity,
                "P(g|g) [%]": gg,
                "P(g|e) [%]": eg,
                "P(e|g) [%]": ge,
                "P(e|e) [%]": ee,
                "snr": snr,
                **asdict(self.log_settings),
            },
        ).squeeze()

        # Plot the data
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(8, 8))

        fig.suptitle("\n".join([
            "Readout Angle Optimization",
            f"drive_frequency = {DRIVE_FREQUENCY:.4g} Hz",
            f"drive_amplitude = {drive_powers_in_v:.4f} V",
            f"drive_pulse_width = {DRIVE_PULSE_WIDTH} ns",
            "",
            f"readout_frequency = {READOUT_FREQUENCY:.4g} Hz",
            f"readout_amplitude = {readout_powers_in_v:.4g} V",
            f"readout_pulse_width = {READOUT_PULSE_WIDTH} ns",
            "",
            f"readout_angle = {READOUT_ANGLE / np.pi} \u03C0 rad",
            "",
            f"n_averages: {N_AVERAGES}",
            f"n_single_shots: {N_SINGLE_SHOTS}",
        ]))

        red = [0.19477124, 0.48650519, 0.71872357, 0.75]
        blue = [0.76724337, 0.23252595, 0.23398693, 0.75]

        ax1.scatter(Ig, Qg, marker=".", alpha=0.1, label="|g⟩", color=blue)
        ax1.scatter(Ie, Qe, marker=".", alpha=0.1, label="|e⟩", color=red)
        ax1.axis("equal")
        ax1.set_xlabel("I (V)")
        ax1.set_ylabel("Q (V)")
        ax1.set_title("Original Single Shot Data")
        ax1.legend(loc="upper right")

        ax2.scatter(
            Ig * np.cos(angle) - Qg * np.sin(angle),
            Ig * np.sin(angle) + Qg * np.cos(angle),
            marker=".",
            alpha=0.1,
            label="|g⟩",
            color=blue,
        )
        ax2.scatter(
            Ie * np.cos(angle) - Qe * np.sin(angle),
            Ie * np.sin(angle) + Qe * np.cos(angle),
            marker=".",
            alpha=0.1,
            label="|e⟩",
            color=red,
        )
        ax2.axis("equal")
        ax2.set_xlabel("I (V)")
        ax2.set_ylabel("Q (V)")
        ax2.set_title(f"Rotated Single Shot Data ({angle / np.pi:.3f}π rad)")
        ax2.legend(loc="upper right")

        ax3.hist(Ig * np.cos(angle) - Qg * np.sin(angle),
                 bins=50,
                 alpha=0.75,
                 label="|g⟩",
                 color=blue)
        ax3.hist(Ie * np.cos(angle) - Qe * np.sin(angle),
                 bins=50,
                 alpha=0.75,
                 label="|e⟩",
                 color=red)
        ax3.axvline(x=threshold,
                    color="k",
                    ls="--",
                    alpha=0.5,
                    label=f"threshold: {threshold:.3g} V")
        ax3.set_xlabel("I (V)")
        ax3.set_title("1D Histogram")
        ax3.legend(loc="lower right")

        ax4.imshow(np.array([[gg, ge], [eg, ee]]), cmap="RdBu_r")
        ax4.set_xticks([0, 1])
        ax4.set_yticks([0, 1])
        ax4.set_xticklabels(labels=["|g>", "|e>"])
        ax4.set_yticklabels(labels=["|g>", "|e>"])
        ax4.set_ylabel("Prepared")
        ax4.set_xlabel("Measured")
        ax4.text(0, 0, f"{100 * gg:.1f}%", ha="center", va="center", color="k")
        ax4.text(1, 0, f"{100 * ge:.1f}%", ha="center", va="center", color="w")
        ax4.text(0, 1, f"{100 * eg:.1f}%", ha="center", va="center", color="w")
        ax4.text(1, 1, f"{100 * ee:.1f}%", ha="center", va="center", color="k")
        ax4.set_title("Fidelities")

        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()

        return dataset


class Fidelity2(QuaMeasurement):
    measurement_type = "multi_tone"

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = MultiToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        tuned_object = MultiToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = MultiToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = MultiToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = MultiToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        if integration_weights := self.sweep_settings.readout_pulse.path_to_integration_weights:
            tuned_object = MultiToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = MultiToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        for i in [1, 2, 3, 4]:
            if rgetattr(self, f'sweep_settings.drive_{i}_pulse') is not None:
                tuned_object = MultiToneTuner.set_drive_if_frequency(
                    tuned_object, i,
                    rgetattr(self,
                             f'sweep_settings.drive_{i}_pulse.frequency'))
                tuned_object = MultiToneTuner.set_drive_lo_frequency(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_mix.frequency'))
                tuned_object = MultiToneTuner.set_drive_if_power(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.power'))
                tuned_object = MultiToneTuner.set_drive_pulse_width(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.width'))
                tuned_object = MultiToneTuner.set_drive_pulse_shape(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.shape'))

        tuned_object = MultiToneTuner.set_n_single_shots(
            tuned_object, self.sweep_settings.sweep.n_single_shots)
        tuned_object = MultiToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = MultiToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = MultiToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        DRIVE_01_IF_POWER = self.get_settings("sweep.drive_1_if_power")[0]
        DRIVE_01_PULSE_SHAPE = self.get_settings("sweep.drive_1_pulse_shape")
        DRIVE_01_PULSE_WIDTH = self.get_settings(
            "sweep.drive_1_pulse_width")[0]

        DRIVE_12_IF_POWER = self.get_settings("sweep.drive_3_if_power")[0]
        DRIVE_12_PULSE_SHAPE = self.get_settings("sweep.drive_3_pulse_shape")
        DRIVE_12_PULSE_WIDTH = self.get_settings(
            "sweep.drive_3_pulse_width")[0]

        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]

        N_SINGLE_SHOTS = self.get_settings("sweep.n_single_shots")
        N_AVERAGES = self.get_settings("sweep.n_averages")
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        with program() as ro_int_angle_opt:
            m = declare(int)  # QUA variable for the averaging loop
            n = declare(int)  # QUA variable for the averaging loop

            Ig = declare(
                fixed
            )  # QUA variable for the 'I' quadrature when the qubit is in |g>
            Qg = declare(
                fixed
            )  # QUA variable for the 'Q' quadrature when the qubit is in |g>
            Ig_st = declare_stream()
            Qg_st = declare_stream()

            If = declare(
                fixed
            )  # QUA variable for the 'I' quadrature when the qubit is in |e>
            Qf = declare(
                fixed
            )  # QUA variable for the 'Q' quadrature when the qubit is in |e>
            If_st = declare_stream()
            Qf_st = declare_stream()

            n_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):
                with for_(m, 0, m < N_SINGLE_SHOTS, m + 1):

                    # Measure the ground state.
                    # Measure the state of the resonator
                    measure(
                        amp(READOUT_IF_POWER) * "readout",
                        "resonator",
                        None,
                        dual_demod.full("cosine", "out1", "sine", "out2", Ig),
                        dual_demod.full("minus_sine", "out1", "cosine", "out2",
                                        Qg),
                    )
                    # Wait for the qubit to decay to the ground state in the case of measurement induced transitions
                    wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                    # Save the 'I' & 'Q' quadratures to their respective streams for the ground state
                    save(Ig, Ig_st)
                    save(Qg, Qg_st)

                    align()  # global align
                    # Play the x180 gate to put the qubit in the excited state
                    play(amp(DRIVE_01_IF_POWER) * DRIVE_01_PULSE_SHAPE,
                         "qubit_1",
                         duration=DRIVE_01_PULSE_WIDTH * unit_tools.ns)
                    align('qubit_1', 'qubit_3')
                    # Play the x180 gate to put the qubit in the excited state
                    play(amp(DRIVE_12_IF_POWER) * DRIVE_12_PULSE_SHAPE,
                         "qubit_3",
                         duration=DRIVE_12_PULSE_WIDTH * unit_tools.ns)

                    # Align the two elements to measure after playing the qubit pulse.
                    align("qubit_3", "resonator")
                    # Measure the state of the resonator
                    measure(
                        amp(READOUT_IF_POWER) * "readout",
                        "resonator",
                        None,
                        dual_demod.full("cosine", "out1", "sine", "out2", If),
                        dual_demod.full("minus_sine", "out1", "cosine", "out2",
                                        Qf),
                    )
                    # Wait for the qubit to decay to the ground state
                    wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                    # Save the 'I' & 'Q' quadratures to their respective streams for the excited state
                    save(If, If_st)
                    save(Qf, Qf_st)

                save(n, n_st)

            with stream_processing():
                n_st.save("iteration")
                # mean values
                Ig_st.buffer(N_SINGLE_SHOTS).average().save("Ig")
                Qg_st.buffer(N_SINGLE_SHOTS).average().save("Qg")
                If_st.buffer(N_SINGLE_SHOTS).average().save("If")
                Qf_st.buffer(N_SINGLE_SHOTS).average().save("Qf")

        return ro_int_angle_opt

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):

        DRIVE_01_FREQUENCY = self.get_settings("sweep.drive_1_if_frequency")[
            0] + self.get_settings("sweep.drive_1_lo_frequency")[0]
        DRIVE_01_IF_POWER = self.get_settings("sweep.drive_1_if_power")[0]
        DRIVE_01_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.qubit_1_pi_wf.samples"))
        DRIVE_01_PULSE_WIDTH = self.get_settings(
            "sweep.drive_1_pulse_width")[0]

        DRIVE_12_FREQUENCY = self.get_settings("sweep.drive_3_if_frequency")[
            0] + self.get_settings("sweep.drive_3_lo_frequency")[0]
        DRIVE_12_IF_POWER = self.get_settings("sweep.drive_3_if_power")[0]
        DRIVE_12_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.qubit_3_pi_wf.samples"))
        DRIVE_12_PULSE_WIDTH = self.get_settings(
            "sweep.drive_3_pulse_width")[0]

        READOUT_FREQUENCY = self.get_settings("sweep.readout_if_frequency")[
            0] + self.get_settings("sweep.readout_lo_frequency")[0]
        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]
        READOUT_IF_POWER_SCALAR = self.get_settings(
            "config.waveforms.ro_wf.sample")

        READOUT_PULSE_WIDTH = self.get_settings("sweep.readout_pulse_width")[0]
        READOUT_ANGLE = self.get_settings("sweep.readout_angle")[0]

        N_AVERAGES = self.get_settings("sweep.n_averages")
        N_SINGLE_SHOTS = self.get_settings("sweep.n_single_shots")

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        Ig = unit_tools.demod2volts(
            res_handles.get("Ig").fetch_all(),
            READOUT_PULSE_WIDTH)  # transpose to get amplitude over axis 0
        Qg = unit_tools.demod2volts(
            res_handles.get("Qg").fetch_all(), READOUT_PULSE_WIDTH)
        Ie = unit_tools.demod2volts(
            res_handles.get("If").fetch_all(), READOUT_PULSE_WIDTH)
        Qe = unit_tools.demod2volts(
            res_handles.get("Qf").fetch_all(), READOUT_PULSE_WIDTH)

        readout_powers_in_v = READOUT_IF_POWER * READOUT_IF_POWER_SCALAR
        drive_1_powers_in_v = DRIVE_01_IF_POWER * round(
            DRIVE_01_IF_POWER_SCALAR, 3)
        drive_2_powers_in_v = DRIVE_12_IF_POWER * round(
            DRIVE_12_IF_POWER_SCALAR, 3)

        signal = Ie - Ig
        signal_without_outliers = signal[np.abs(signal -
                                                np.average(signal)) < (
                                                    2 * np.std(signal))]
        snr = np.average(signal_without_outliers**
                         2) / np.var(signal_without_outliers)

        angle, threshold, fidelity, gg, ge, eg, ee = two_state_discriminator(
            Ig, Qg, Ie, Qe, b_print=True, b_plot=False)

        dims = [
            "single_shot_index",
        ]

        dataset = xr.Dataset(
            data_vars={
                "Ig (V)": (dims, Ig),
                "Qg (V)": (dims, Qg),
                "If (V)": (dims, Ie),
                "Qf (V)": (dims, Qe),
            },
            coords={
                "single_shot_index":
                (("single_shot_index", np.arange(N_SINGLE_SHOTS)))
            },
            attrs={
                "angle [rad]": angle,
                "threshold [V]": threshold,
                "fidelity [%]": fidelity,
                "P(g|g) [%]": gg,
                "P(g|e) [%]": eg,
                "P(e|g) [%]": ge,
                "P(e|e) [%]": ee,
                "snr": snr,
                **asdict(self.log_settings),
            },
        ).squeeze()

        # Plot the data
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(8, 8))

        fig.suptitle("\n".join([
            "Readout Angle Optimization",
            f"drive_01_frequency = {DRIVE_01_FREQUENCY:.4g} Hz",
            f"drive_01_amplitude = {drive_1_powers_in_v:.4f} V",
            f"drive_01_pulse_width = {DRIVE_01_PULSE_WIDTH} ns",
            "",
            f"drive_12_frequency = {DRIVE_12_FREQUENCY:.4g} Hz",
            f"drive_12_amplitude = {drive_2_powers_in_v:.4f} V",
            f"drive_12_pulse_width = {DRIVE_12_PULSE_WIDTH} ns",
            "",
            f"readout_frequency = {READOUT_FREQUENCY:.4g} Hz",
            f"readout_amplitude = {readout_powers_in_v:.4g} V",
            f"readout_pulse_width = {READOUT_PULSE_WIDTH} ns",
            "",
            f"readout_angle = {READOUT_ANGLE / np.pi} \u03C0 rad",
            "",
            f"n_averages: {N_AVERAGES}",
            f"n_single_shots: {N_SINGLE_SHOTS}",
        ]))

        red = [0.19477124, 0.48650519, 0.71872357, 0.75]
        blue = [0.76724337, 0.23252595, 0.23398693, 0.75]

        ax1.scatter(Ig, Qg, marker=".", alpha=0.1, label="|g⟩", color=blue)
        ax1.scatter(Ie, Qe, marker=".", alpha=0.1, label="|e⟩", color=red)
        ax1.axis("equal")
        ax1.set_xlabel("I (V)")
        ax1.set_ylabel("Q (V)")
        ax1.set_title("Original Single Shot Data")
        ax1.legend(loc="upper right")

        ax2.scatter(
            Ig * np.cos(angle) - Qg * np.sin(angle),
            Ig * np.sin(angle) + Qg * np.cos(angle),
            marker=".",
            alpha=0.1,
            label="|g⟩",
            color=blue,
        )
        ax2.scatter(
            Ie * np.cos(angle) - Qe * np.sin(angle),
            Ie * np.sin(angle) + Qe * np.cos(angle),
            marker=".",
            alpha=0.1,
            label="|f⟩",
            color=red,
        )
        ax2.axis("equal")
        ax2.set_xlabel("I (V)")
        ax2.set_ylabel("Q (V)")
        ax2.set_title(f"Rotated Single Shot Data ({angle / np.pi:.3f}π rad)")
        ax2.legend(loc="upper right")

        ax3.hist(Ig * np.cos(angle) - Qg * np.sin(angle),
                 bins=50,
                 alpha=0.75,
                 label="|g⟩",
                 color=blue)
        ax3.hist(Ie * np.cos(angle) - Qe * np.sin(angle),
                 bins=50,
                 alpha=0.75,
                 label="|f⟩",
                 color=red)
        ax3.axvline(x=threshold,
                    color="k",
                    ls="--",
                    alpha=0.5,
                    label=f"threshold: {threshold:.3g} V")
        ax3.set_xlabel("I (V)")
        ax3.set_title("1D Histogram")
        ax3.legend(loc="lower right")

        ax4.imshow(np.array([[gg, ge], [eg, ee]]), cmap="RdBu_r")
        ax4.set_xticks([0, 1])
        ax4.set_yticks([0, 1])
        ax4.set_xticklabels(labels=["|g>", "|e>"])
        ax4.set_yticklabels(labels=["|g>", "|e>"])
        ax4.set_ylabel("Prepared")
        ax4.set_xlabel("Measured")
        ax4.text(0, 0, f"{100 * gg:.1f}%", ha="center", va="center", color="k")
        ax4.text(1, 0, f"{100 * ge:.1f}%", ha="center", va="center", color="w")
        ax4.text(0, 1, f"{100 * eg:.1f}%", ha="center", va="center", color="w")
        ax4.text(1, 1, f"{100 * ee:.1f}%", ha="center", va="center", color="k")
        ax4.set_title("Fidelities")

        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()

        return dataset


if __name__ == "__main__":
    pass
