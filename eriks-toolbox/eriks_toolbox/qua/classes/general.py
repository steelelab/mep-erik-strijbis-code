import os
from copy import deepcopy

from eriks_stlab_toolbox.general.measurement import MultiToneSettings
from eriks_stlab_toolbox.general.utilities import rgetattr
from eriks_stlab_toolbox.qua.classes.measurement import QuaMeasurement
from eriks_stlab_toolbox.qua.configuration import MultiToneTuner

from qualang_tools.units import unit

unit_tools = unit(coerce_to_integer=True)

# TODO: if else tuned_settings / settings -> tuned_settings only


class GeneralMeasurement(QuaMeasurement):
    measurement_type = "multi_tone"

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = MultiToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        for i in [1, 2, 3, 4]:
            if rgetattr(self, f'sweep_settings.drive_{i}_pulse') is not None:
                tuned_object = MultiToneTuner.set_drive_if_frequency(
                    tuned_object, i,
                    rgetattr(self,
                             f'sweep_settings.drive_{i}_pulse.frequency'))
                tuned_object = MultiToneTuner.set_drive_lo_frequency(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_mix.frequency'))
                tuned_object = MultiToneTuner.set_drive_if_power(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.power'))
                tuned_object = MultiToneTuner.set_drive_pulse_width(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.width'))
                tuned_object = MultiToneTuner.set_drive_pulse_shape(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.shape'))

        tuned_object = MultiToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = MultiToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = MultiToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = MultiToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        if integration_weights := self.sweep_settings.readout_pulse.path_to_integration_weights:
            tuned_object = MultiToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = MultiToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        tuned_object = MultiToneTuner.set_n_single_shots(
            tuned_object, self.sweep_settings.sweep.n_single_shots)
        tuned_object = MultiToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = MultiToneTuner.set_idle_time(
            tuned_object, self.sweep_settings.sweep.idle_time)
        tuned_object = MultiToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = MultiToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        print(
            'obj.make_measurement() - and obj.do() - not implemented for GeneralMeasurement'
        )
        print(
            'assign obj.measurement_program manually and assign obj.measurement_result = obj.do_measurement()'
        )

    def save_measurement(self):
        print('obj.save_measurement() not implemented for GeneralMeasurment')
        print('save obj.measruement_result manually')


if __name__ == "__main__":
    pass
