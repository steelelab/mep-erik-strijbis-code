# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"></ul></div>

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"></ul></div>

import os
from copy import deepcopy
from dataclasses import asdict

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from eriks_stlab_toolbox.general.measurement import (MultiToneSettings,
                                                     TwoToneSettings)
from eriks_stlab_toolbox.general.utilities import rgetattr, x_to_db
from eriks_stlab_toolbox.qua.classes.measurement import QuaMeasurement
from eriks_stlab_toolbox.qua.configuration import MultiToneTuner, TwoToneTuner
from qm.qua import (align, amp, declare, declare_stream, dual_demod, fixed,
                    for_, if_, measure, play, program, save, stream_processing,
                    update_frequency, wait)
from qualang_tools.loops import from_array
from qualang_tools.units import unit

unit_tools = unit(coerce_to_integer=True)

# TODO: if else tuned_settings / settings -> tuned_settings only


class RamseyChevron(QuaMeasurement):
    measurement_type = "two_tone"

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = TwoToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        tuned_object = TwoToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = TwoToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = TwoToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = TwoToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        if integration_weights := self.sweep_settings.readout_pulse.path_to_integration_weights:
            tuned_object = TwoToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = TwoToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        tuned_object = TwoToneTuner.set_drive_if_frequency(
            tuned_object, self.sweep_settings.drive_pulse.frequency)
        tuned_object = TwoToneTuner.set_drive_lo_frequency(
            tuned_object, self.sweep_settings.drive_mix.frequency)
        tuned_object = TwoToneTuner.set_drive_if_power(
            tuned_object, self.sweep_settings.drive_pulse.power)
        tuned_object = TwoToneTuner.set_drive_pulse_width(
            tuned_object, self.sweep_settings.drive_pulse.width)
        tuned_object = TwoToneTuner.set_drive_pulse_shape(
            tuned_object, self.sweep_settings.drive_pulse.shape)

        tuned_object = TwoToneTuner.set_idle_time(
            tuned_object, self.sweep_settings.sweep.idle_time)
        tuned_object = TwoToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = TwoToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = TwoToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        DRIVE_IF_FREQUENCY = np.array(
            self.get_settings("sweep.drive_if_frequency"))
        DRIVE_IF_POWER = np.array(self.get_settings("sweep.drive_if_power"))
        DRIVE_PULSE_SHAPE = self.get_settings("sweep.drive_pulse_shape")
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_IF_FREQUENCY = np.array(
            self.get_settings("sweep.readout_if_frequency"))
        READOUT_IF_POWER = np.array(
            self.get_settings("sweep.readout_if_power"))

        N_AVERAGES = self.get_settings("sweep.n_averages")
        IDLE_TIME = np.array(self.get_settings("sweep.idle_time"))
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        with program() as ramsey_chevron:
            n = declare(int)  # averaging
            t = declare(int)  # pulse width
            f_readout = declare(int)  # resonator frequencies
            f_drive = declare(int)  # qubit frequencies
            a_readout = declare(fixed)  # resonator pulse amplitudes
            a_drive = declare(fixed)  # qubit pulse amplitudes

            I = declare(fixed)
            Q = declare(fixed)

            I_st = declare_stream()
            Q_st = declare_stream()
            n_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):
                with for_(*from_array(t, IDLE_TIME * unit_tools.ns)):
                    with for_(*from_array(f_readout, READOUT_IF_FREQUENCY)):
                        update_frequency("resonator", f_readout)
                        with for_(*from_array(f_drive, DRIVE_IF_FREQUENCY)):
                            update_frequency("qubit", f_drive)

                            with for_(
                                    *from_array(a_readout, READOUT_IF_POWER)):
                                with for_(
                                        *from_array(a_drive, DRIVE_IF_POWER)):
                                    play(DRIVE_PULSE_SHAPE * amp(a_drive / 2),
                                         "qubit",
                                         duration=DRIVE_PULSE_WIDTH *
                                         unit_tools.ns)

                                    with if_(t > 0):
                                        wait(t, "qubit")

                                    play(DRIVE_PULSE_SHAPE * amp(a_drive / 2),
                                         "qubit",
                                         duration=DRIVE_PULSE_WIDTH *
                                         unit_tools.ns)

                                    align("qubit", "resonator")

                                    measure(
                                        "readout" * amp(a_readout),
                                        "resonator",
                                        None,
                                        dual_demod.full(
                                            "cosine", "out1", "sine", "out2",
                                            I),
                                        dual_demod.full(
                                            "minus_sine", "out1", "cosine",
                                            "out2", Q),
                                    )

                                    wait(WAIT_BETWEEN * unit_tools.ns,
                                         "resonator")

                                    save(I, I_st)
                                    save(Q, Q_st)
                save(n, n_st)

            with stream_processing():
                I_st.buffer(
                    len(DRIVE_IF_POWER)).buffer(len(READOUT_IF_POWER)).buffer(
                        len(DRIVE_IF_FREQUENCY)).buffer(
                            len(READOUT_IF_FREQUENCY)).buffer(
                                len(IDLE_TIME)).average().save("I")
                Q_st.buffer(
                    len(DRIVE_IF_POWER)).buffer(len(READOUT_IF_POWER)).buffer(
                        len(DRIVE_IF_FREQUENCY)).buffer(
                            len(READOUT_IF_FREQUENCY)).buffer(
                                len(IDLE_TIME)).average().save("Q")
                n_st.save("iteration")

        return ramsey_chevron

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        DRIVE_FREQUENCY = np.array(
            self.get_settings("sweep.drive_if_frequency")) + np.array(
                self.get_settings("sweep.drive_lo_frequency"))
        DRIVE_IF_POWER = np.array(self.get_settings("sweep.drive_if_power"))
        DRIVE_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.pi_wf.samples"))
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_FREQUENCY = np.array(
            self.get_settings("sweep.readout_if_frequency")) + np.array(
                self.get_settings("sweep.readout_lo_frequency"))
        READOUT_IF_POWER = np.array(
            self.get_settings("sweep.readout_if_power"))
        READOUT_IF_POWER_SCALAR = self.get_settings(
            "config.waveforms.ro_wf.sample")
        READOUT_PULSE_WIDTH = self.get_settings("sweep.readout_pulse_width")[0]

        IDLE_TIME = np.array(self.get_settings("sweep.idle_time"))

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        i = unit_tools.demod2volts(
            res_handles.get("I").fetch_all(), READOUT_PULSE_WIDTH)
        q = unit_tools.demod2volts(
            res_handles.get("Q").fetch_all(), READOUT_PULSE_WIDTH)
        complex_signal = i + 1j * q

        readout_powers_in_v = READOUT_IF_POWER * READOUT_IF_POWER_SCALAR
        drive_powers_in_v = DRIVE_IF_POWER * round(DRIVE_IF_POWER_SCALAR, 3)

        s21 = complex_signal / readout_powers_in_v[:, np.
                                                   newaxis]  # add axis to divide along readout power axis

        phase = np.angle(complex_signal)

        if len(READOUT_FREQUENCY) > 1:
            unwrapped_phase = np.unwrap(phase, axis=1)
            group_delay = -1 * np.gradient(
                unwrapped_phase,
                2 * np.pi * np.diff(READOUT_FREQUENCY)[0],
                axis=1)

        else:
            unwrapped_phase = phase
            group_delay = np.zeros(phase.shape)

        dims = [
            "idle_time (ns)",
            "readout_frequency (Hz)",
            "drive_frequency (Hz)",
            "readout_amplitude (V)",
            "drive_amplitude (V)",
        ]

        dataset = xr.Dataset(
            data_vars={
                "real [V]": (dims, i),
                "imaginary [V]": (dims, q),
                "magnitude [V]": (dims, np.abs(complex_signal)),
                "phase [rad]": (dims, phase),
                "unwrapped_phase [rad]": (dims, unwrapped_phase),
                "group_delay [s]": (dims, group_delay),
                "s21_real [_]": (dims, s21.real),
                "s21_imaginary [_]": (dims, s21.imag),
                "s21_magnitude [dB]": (dims, x_to_db(np.abs(s21))),
            },
            coords={
                "idle_time (ns)": (("idle_time (ns)", IDLE_TIME)),
                "readout_frequency (Hz)":
                (("readout_frequency (Hz)", READOUT_FREQUENCY)),
                "drive_frequency (Hz)":
                (("drive_frequency (Hz)", DRIVE_FREQUENCY)),
                "readout_amplitude (V)":
                (("readout_amplitude (V)", readout_powers_in_v)),
                "drive_amplitude (V)":
                (("drive_amplitude (V)", drive_powers_in_v / 2)),
            },
            attrs=asdict(self.log_settings),
        ).squeeze()

        dataset.to_netcdf(f"{self.output_path}/{self.name}/{self.name}.hdf5")

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8))

        fig.suptitle("\n".join([
            "Ramsey Chevron",
            f"drive_pulse_width = {DRIVE_PULSE_WIDTH} ns",
            f"readout_pulse_width = {READOUT_PULSE_WIDTH} ns",
        ]))

        ax1.set_title("\n".join([
            "drive_frequency vs. drive_amplitude",
            f"@ drive_amplitude = {drive_powers_in_v[-1]:.4f} / 2 V",
            f"@ readout_frequency = {READOUT_FREQUENCY[-1]:.4g} Hz",
            f"@ readout_amplitude = {readout_powers_in_v[-1]:.4f} V",
        ]))
        X, Y = np.meshgrid(DRIVE_FREQUENCY, IDLE_TIME)
        im = ax1.pcolor(X,
                        Y,
                        np.abs(complex_signal[:, -1, :, -1, -1]),
                        cmap="RdBu_r")

        cbar = plt.colorbar(im, ax=ax1)
        cbar.ax.get_yaxis().labelpad = 15
        cbar.ax.set_ylabel("|I + iQ| [V]", rotation=270)

        ax1.set_xlabel("Drive Frequency [Hz]")
        ax1.set_ylabel("Idle Time [ns]")

        ax2.set_title(f"linecut @ idle_time = {IDLE_TIME[-1]:.0f} ns")
        ax2.plot(DRIVE_FREQUENCY, np.abs(complex_signal[:, -1, :, -1, -1])[-1])

        ax2.set_xlabel("Drive Frequency [Hz]")
        ax2.set_ylabel("|I + iQ| [V]")

        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()

        return dataset


class RamseyChevron2(QuaMeasurement):
    measurement_type = "multi_tone"

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = MultiToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        tuned_object = MultiToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = MultiToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = MultiToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = MultiToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        if integration_weights := self.sweep_settings.readout_pulse.path_to_integration_weights:
            tuned_object = MultiToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = MultiToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        for i in [1, 2, 3, 4]:
            if rgetattr(self, f'sweep_settings.drive_{i}_pulse') is not None:
                tuned_object = MultiToneTuner.set_drive_if_frequency(
                    tuned_object, i,
                    rgetattr(self,
                             f'sweep_settings.drive_{i}_pulse.frequency'))
                tuned_object = MultiToneTuner.set_drive_lo_frequency(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_mix.frequency'))
                tuned_object = MultiToneTuner.set_drive_if_power(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.power'))
                tuned_object = MultiToneTuner.set_drive_pulse_width(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.width'))
                tuned_object = MultiToneTuner.set_drive_pulse_shape(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.shape'))

        tuned_object = MultiToneTuner.set_idle_time(
            tuned_object, self.sweep_settings.sweep.idle_time)
        tuned_object = MultiToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = MultiToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = MultiToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        DRIVE_01_IF_POWER = self.get_settings("sweep.drive_1_if_power")[0]
        DRIVE_01_PULSE_SHAPE = self.get_settings("sweep.drive_1_pulse_shape")
        DRIVE_01_PULSE_WIDTH = self.get_settings(
            "sweep.drive_1_pulse_width")[0]

        DRIVE_12_IF_FREQUENCY = np.array(
            self.get_settings("sweep.drive_3_if_frequency"))
        DRIVE_12_IF_POWER = np.array(
            self.get_settings("sweep.drive_3_if_power"))

        DRIVE_12_PULSE_SHAPE = self.get_settings("sweep.drive_3_pulse_shape")
        DRIVE_12_PULSE_WIDTH = self.get_settings(
            "sweep.drive_3_pulse_width")[0]

        READOUT_IF_FREQUENCY = np.array(
            self.get_settings("sweep.readout_if_frequency"))
        READOUT_IF_POWER = np.array(
            self.get_settings("sweep.readout_if_power"))

        N_AVERAGES = self.get_settings("sweep.n_averages")
        IDLE_TIME = np.array(self.get_settings("sweep.idle_time"))
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        with program() as ramsey_chevron:
            n = declare(int)  # averaging
            t = declare(int)  # pulse width
            f_readout = declare(int)  # resonator frequencies
            f_drive = declare(int)  # qubit frequencies
            a_readout = declare(fixed)  # resonator pulse amplitudes
            a_drive = declare(fixed)  # qubit pulse amplitudes

            I = declare(fixed)
            Q = declare(fixed)

            I_st = declare_stream()
            Q_st = declare_stream()
            n_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):
                with for_(*from_array(t, IDLE_TIME * unit_tools.ns)):
                    with for_(*from_array(f_readout, READOUT_IF_FREQUENCY)):
                        update_frequency("resonator", f_readout)
                        with for_(*from_array(f_drive, DRIVE_12_IF_FREQUENCY)):
                            update_frequency("qubit_3", f_drive)

                            with for_(
                                    *from_array(a_readout, READOUT_IF_POWER)):
                                with for_(*from_array(a_drive,
                                                      DRIVE_12_IF_POWER)):

                                    play(DRIVE_01_PULSE_SHAPE *
                                         amp(DRIVE_01_IF_POWER),
                                         "qubit_1",
                                         duration=DRIVE_01_PULSE_WIDTH *
                                         unit_tools.ns)

                                    align("qubit_1", "qubit_3")

                                    play(DRIVE_12_PULSE_SHAPE *
                                         amp(a_drive / 2),
                                         "qubit_3",
                                         duration=DRIVE_12_PULSE_WIDTH *
                                         unit_tools.ns)

                                    with if_(t > 0):
                                        wait(t, "qubit_3")

                                    play(DRIVE_12_PULSE_SHAPE *
                                         amp(a_drive / 2),
                                         "qubit_3",
                                         duration=DRIVE_12_PULSE_WIDTH *
                                         unit_tools.ns)

                                    align("qubit_3", "resonator")

                                    measure(
                                        "readout" * amp(a_readout),
                                        "resonator",
                                        None,
                                        dual_demod.full(
                                            "cosine", "out1", "sine", "out2",
                                            I),
                                        dual_demod.full(
                                            "minus_sine", "out1", "cosine",
                                            "out2", Q),
                                    )

                                    wait(WAIT_BETWEEN * unit_tools.ns,
                                         "resonator")

                                    save(I, I_st)
                                    save(Q, Q_st)
                save(n, n_st)

            with stream_processing():
                I_st.buffer(len(DRIVE_12_IF_POWER)).buffer(
                    len(READOUT_IF_POWER)).buffer(
                        len(DRIVE_12_IF_FREQUENCY)).buffer(
                            len(READOUT_IF_FREQUENCY)).buffer(
                                len(IDLE_TIME)).average().save("I")
                Q_st.buffer(len(DRIVE_12_IF_POWER)).buffer(
                    len(READOUT_IF_POWER)).buffer(
                        len(DRIVE_12_IF_FREQUENCY)).buffer(
                            len(READOUT_IF_FREQUENCY)).buffer(
                                len(IDLE_TIME)).average().save("Q")
                n_st.save("iteration")

        return ramsey_chevron

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        DRIVE_01_FREQUENCY = np.array(
            self.get_settings("sweep.drive_1_if_frequency")) + np.array(
                self.get_settings("sweep.drive_1_lo_frequency"))
        DRIVE_01_IF_POWER = np.array(
            self.get_settings("sweep.drive_1_if_power"))
        DRIVE_01_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.qubit_1_pi_wf.samples"))
        DRIVE_01_PULSE_WIDTH = np.array(
            self.get_settings("sweep.drive_1_pulse_width"))

        DRIVE_12_FREQUENCY = np.array(
            self.get_settings("sweep.drive_3_if_frequency")) + np.array(
                self.get_settings("sweep.drive_3_lo_frequency"))
        DRIVE_12_IF_POWER = np.array(
            self.get_settings("sweep.drive_3_if_power"))
        DRIVE_12_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.qubit_3_pi_wf.samples"))
        DRIVE_12_PULSE_WIDTH = np.array(
            self.get_settings("sweep.drive_3_pulse_width"))

        READOUT_FREQUENCY = np.array(
            self.get_settings("sweep.readout_if_frequency")) + np.array(
                self.get_settings("sweep.readout_lo_frequency"))
        READOUT_IF_POWER = np.array(
            self.get_settings("sweep.readout_if_power"))
        READOUT_IF_POWER_SCALAR = self.get_settings(
            "config.waveforms.ro_wf.sample")
        READOUT_PULSE_WIDTH = self.get_settings("sweep.readout_pulse_width")[0]

        IDLE_TIME = np.array(self.get_settings("sweep.idle_time"))

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        i = unit_tools.demod2volts(
            res_handles.get("I").fetch_all(), READOUT_PULSE_WIDTH)
        q = unit_tools.demod2volts(
            res_handles.get("Q").fetch_all(), READOUT_PULSE_WIDTH)
        complex_signal = i + 1j * q

        readout_powers_in_v = READOUT_IF_POWER * READOUT_IF_POWER_SCALAR
        drive_01_powers_in_v = DRIVE_01_IF_POWER * round(
            DRIVE_01_IF_POWER_SCALAR, 3)
        drive_12_powers_in_v = DRIVE_12_IF_POWER * round(
            DRIVE_12_IF_POWER_SCALAR, 3)

        s21 = complex_signal / readout_powers_in_v[:, np.
                                                   newaxis]  # add axis to divide along readout power axis

        phase = np.angle(complex_signal)

        if len(READOUT_FREQUENCY) > 1:
            unwrapped_phase = np.unwrap(phase, axis=1)
            group_delay = -1 * np.gradient(
                unwrapped_phase,
                2 * np.pi * np.diff(READOUT_FREQUENCY)[0],
                axis=1)

        else:
            unwrapped_phase = phase
            group_delay = np.zeros(phase.shape)

        dims = [
            "idle_time (ns)",
            "readout_frequency (Hz)",
            "drive_frequency (Hz)",
            "readout_amplitude (V)",
            "drive_amplitude (V)",
        ]

        dataset = xr.Dataset(
            data_vars={
                "real [V]": (dims, i),
                "imaginary [V]": (dims, q),
                "magnitude [V]": (dims, np.abs(complex_signal)),
                "phase [rad]": (dims, phase),
                "unwrapped_phase [rad]": (dims, unwrapped_phase),
                "group_delay [s]": (dims, group_delay),
                "s21_real [_]": (dims, s21.real),
                "s21_imaginary [_]": (dims, s21.imag),
                "s21_magnitude [dB]": (dims, x_to_db(np.abs(s21))),
            },
            coords={
                "idle_time (ns)": (("idle_time (ns)", IDLE_TIME)),
                "readout_frequency (Hz)":
                (("readout_frequency (Hz)", READOUT_FREQUENCY)),
                "drive_frequency (Hz)":
                (("drive_frequency (Hz)", DRIVE_12_FREQUENCY)),
                "readout_amplitude (V)":
                (("readout_amplitude (V)", readout_powers_in_v)),
                "drive_amplitude (V)":
                (("drive_amplitude (V)", drive_12_powers_in_v / 2)),
            },
            attrs=asdict(self.log_settings),
        ).squeeze()

        dataset.to_netcdf(f"{self.output_path}/{self.name}/{self.name}.hdf5")

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8))

        fig.suptitle("\n".join([
            "Ramsey Chevron",
            f"drive_01_frequency = {DRIVE_01_FREQUENCY[-1]:.4g} Hz",
            f"drive_01_amplitude = {drive_01_powers_in_v[-1]:.4f} V",
            f"drive_01_pulse_width = {DRIVE_01_PULSE_WIDTH[-1]:.0f} ns",
            "",
            f"drive_12_pulse_width = {DRIVE_12_PULSE_WIDTH[-1]:.0f} ns",
            "",
            f"readout_pulse_width = {READOUT_PULSE_WIDTH} ns",
        ]))

        ax1.set_title("\n".join([
            "drive_frequency vs. drive_amplitude",
            f"@ drive_12_amplitude = {drive_12_powers_in_v[-1]:.4f} / 2 V",
            "",
            f"@ readout_frequency = {READOUT_FREQUENCY[-1]:.4g} Hz",
            f"@ readout_amplitude = {readout_powers_in_v[-1]:.4f} V",
        ]))
        X, Y = np.meshgrid(DRIVE_12_FREQUENCY, IDLE_TIME)
        im = ax1.pcolor(X,
                        Y,
                        np.abs(complex_signal[:, -1, :, -1, -1]),
                        cmap="RdBu_r")

        cbar = plt.colorbar(im, ax=ax1)
        cbar.ax.get_yaxis().labelpad = 15
        cbar.ax.set_ylabel("|I + iQ| [V]", rotation=270)

        ax1.set_xlabel("Drive Frequency [Hz]")
        ax1.set_ylabel("Idle Time [ns]")

        ax2.set_title(f"linecut @ idle_time = {IDLE_TIME[-1]:.0f} ns")
        ax2.plot(DRIVE_12_FREQUENCY,
                 np.abs(complex_signal[:, -1, :, -1, -1])[-1])

        ax2.set_xlabel("Drive Frequency [Hz]")
        ax2.set_ylabel("|I + iQ| [V]")

        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()

        return dataset


if __name__ == "__main__":
    pass
