import os
from copy import deepcopy
from dataclasses import asdict

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from eriks_stlab_toolbox.general.measurement import TwoToneSettings
from eriks_stlab_toolbox.general.utilities.classes import SignalSmoother
from qm.qua import (align, amp, assign, declare, declare_stream, demod, fixed,
                    for_, measure, play, program, save, stream_processing,
                    wait)
from eriks_stlab_toolbox.qua.classes.measurement import QuaMeasurement
from eriks_stlab_toolbox.qua.configuration import TwoToneTuner
from qualang_tools.units import unit

unit_tools = unit(coerce_to_integer=True)


class ReadoutIntegrationWeightsOptimizer(QuaMeasurement):
    measurement_type = "two_tone"

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = TwoToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        tuned_object = TwoToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = TwoToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = TwoToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = TwoToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        if integration_weights := self.sweep_settings.readout_pulse.path_to_integration_weights:
            tuned_object = TwoToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = TwoToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        tuned_object = TwoToneTuner.set_drive_if_frequency(
            tuned_object, self.sweep_settings.drive_pulse.frequency)
        tuned_object = TwoToneTuner.set_drive_lo_frequency(
            tuned_object, self.sweep_settings.drive_mix.frequency)
        tuned_object = TwoToneTuner.set_drive_if_power(
            tuned_object, self.sweep_settings.drive_pulse.power)
        tuned_object = TwoToneTuner.set_drive_pulse_width(
            tuned_object, self.sweep_settings.drive_pulse.width)
        tuned_object = TwoToneTuner.set_drive_pulse_shape(
            tuned_object, self.sweep_settings.drive_pulse.shape)

        tuned_object = TwoToneTuner.set_n_single_shots(
            tuned_object, self.sweep_settings.sweep.n_single_shots)
        tuned_object = TwoToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = TwoToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = TwoToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        DRIVE_IF_POWER = self.get_settings("sweep.drive_if_power")[0]
        DRIVE_PULSE_SHAPE = self.get_settings("sweep.drive_pulse_shape")
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]
        READOUT_PULSE_WIDTH = np.array(
            self.get_settings("sweep.readout_pulse_width"))

        N_SINGLE_SHOTS = self.get_settings("sweep.n_single_shots")
        N_AVERAGES = self.get_settings("sweep.n_averages")
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        with program() as ro_int_weight_opt:
            m = declare(int)  # QUA variable for the averaging loop
            n = declare(int)

            II = declare(fixed, size=len(READOUT_PULSE_WIDTH))
            IQ = declare(fixed, size=len(READOUT_PULSE_WIDTH))
            QI = declare(fixed, size=len(READOUT_PULSE_WIDTH))
            QQ = declare(fixed, size=len(READOUT_PULSE_WIDTH))

            I = declare(fixed, size=len(READOUT_PULSE_WIDTH))
            Q = declare(fixed, size=len(READOUT_PULSE_WIDTH))

            ind = declare(int)

            n_st = declare_stream()
            Ig_st = declare_stream()
            Qg_st = declare_stream()
            Ie_st = declare_stream()
            Qe_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):
                with for_(m, 0, m < N_SINGLE_SHOTS, m + 1):
                    # Measure the ground state.
                    measure(
                        amp(READOUT_IF_POWER) * "readout",
                        "resonator",
                        None,
                        demod.sliced(
                            "cosine", II,
                            int(
                                np.diff(READOUT_PULSE_WIDTH)[0] *
                                unit_tools.ns), "out1"),
                        demod.sliced(
                            "sine", IQ,
                            int(
                                np.diff(READOUT_PULSE_WIDTH)[0] *
                                unit_tools.ns), "out2"),
                        demod.sliced(
                            "minus_sine", QI,
                            int(
                                np.diff(READOUT_PULSE_WIDTH)[0] *
                                unit_tools.ns), "out1"),
                        demod.sliced(
                            "cosine", QQ,
                            int(
                                np.diff(READOUT_PULSE_WIDTH)[0] *
                                unit_tools.ns), "out2"),
                    )
                    # Save the QUA vectors to their corresponding streams
                    with for_(ind, 0, ind < len(READOUT_PULSE_WIDTH), ind + 1):
                        assign(I[ind], II[ind] + IQ[ind])
                        save(I[ind], Ig_st)
                        assign(Q[ind], QQ[ind] + QI[ind])
                        save(Q[ind], Qg_st)
                    # Wait for the qubit to decay to the ground state
                    wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                    align()

                    # Measure the excited state.
                    play(
                        amp(DRIVE_IF_POWER) * DRIVE_PULSE_SHAPE,
                        "qubit",
                        duration=DRIVE_PULSE_WIDTH * unit_tools.ns,
                    )
                    align("qubit", "resonator")
                    measure(
                        amp(READOUT_IF_POWER) * "readout",
                        "resonator",
                        None,
                        demod.sliced(
                            "cosine", II,
                            int(
                                np.diff(READOUT_PULSE_WIDTH)[0] *
                                unit_tools.ns), "out1"),
                        demod.sliced(
                            "sine", IQ,
                            int(
                                np.diff(READOUT_PULSE_WIDTH)[0] *
                                unit_tools.ns), "out2"),
                        demod.sliced(
                            "minus_sine", QI,
                            int(
                                np.diff(READOUT_PULSE_WIDTH)[0] *
                                unit_tools.ns), "out1"),
                        demod.sliced(
                            "cosine", QQ,
                            int(
                                np.diff(READOUT_PULSE_WIDTH)[0] *
                                unit_tools.ns), "out2"),
                    )
                    # Save the QUA vectors to their corresponding streams
                    with for_(ind, 0, ind < len(READOUT_PULSE_WIDTH), ind + 1):
                        assign(I[ind], II[ind] + IQ[ind])
                        save(I[ind], Ie_st)
                        assign(Q[ind], QQ[ind] + QI[ind])
                        save(Q[ind], Qe_st)

                    # Wait for the qubit to decay to the ground state
                    wait(WAIT_BETWEEN * unit_tools.ns, "resonator")
                    # Save the averaging iteration to get the progress bar
                    save(n, n_st)

            with stream_processing():
                n_st.save("iteration")
                # mean values
                Ig_st.buffer(len(READOUT_PULSE_WIDTH)).buffer(
                    N_SINGLE_SHOTS).average().save("Ig")
                Qg_st.buffer(len(READOUT_PULSE_WIDTH)).buffer(
                    N_SINGLE_SHOTS).average().save("Qg")
                Ie_st.buffer(len(READOUT_PULSE_WIDTH)).buffer(
                    N_SINGLE_SHOTS).average().save("Ie")
                Qe_st.buffer(len(READOUT_PULSE_WIDTH)).buffer(
                    N_SINGLE_SHOTS).average().save("Qe")

        return ro_int_weight_opt

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        DRIVE_FREQUENCY = self.get_settings("sweep.drive_if_frequency")[
            0] + self.get_settings("sweep.drive_lo_frequency")[0]
        DRIVE_IF_POWER = self.get_settings("sweep.drive_if_power")[0]
        DRIVE_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.pi_wf.samples"))
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_FREQUENCY = self.get_settings("sweep.readout_if_frequency")[
            0] + self.get_settings("sweep.readout_lo_frequency")[0]
        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]
        READOUT_IF_POWER_SCALAR = self.get_settings(
            "config.waveforms.ro_wf.sample")
        READOUT_PULSE_WIDTH = np.array(
            self.get_settings("sweep.readout_pulse_width"))

        N_AVERAGES = self.get_settings("sweep.n_averages")
        N_SINGLE_SHOTS = self.get_settings("sweep.n_single_shots")

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        Ig = unit_tools.demod2volts(
            res_handles.get("Ig").fetch_all(),
            READOUT_PULSE_WIDTH).T  # transpose to get amplitude over axis 0
        Qg = unit_tools.demod2volts(
            res_handles.get("Qg").fetch_all(), READOUT_PULSE_WIDTH).T
        Ie = unit_tools.demod2volts(
            res_handles.get("Ie").fetch_all(), READOUT_PULSE_WIDTH).T
        Qe = unit_tools.demod2volts(
            res_handles.get("Qe").fetch_all(), READOUT_PULSE_WIDTH).T

        readout_powers_in_v = READOUT_IF_POWER * READOUT_IF_POWER_SCALAR
        drive_powers_in_v = DRIVE_IF_POWER * round(DRIVE_IF_POWER_SCALAR, 3)

        signal = SignalSmoother(np.abs((Ie + 1j * Qe) - (Ig + 1j * Qg)))

        normalized_signal = signal.smoothed_signal / np.linalg.norm(
            signal.smoothed_signal)
        optimized_weights = normalized_signal / np.abs(normalized_signal).max()

        dims = [
            "readout_time (ns)",
            "single_shot_index",
        ]

        dataset = xr.Dataset(data_vars={
            "Ig (V)": (dims, Ig),
            "Qg (V)": (dims, Qg),
            "Ie (V)": (dims, Ie),
            "Qe (V)": (dims, Qe),
            "signal (V)": (dims, signal.signal),
            "smoothed_signal (V)":
            ("readout_time (ns)", signal.smoothed_signal),
            "optimized_weights": ("readout_time (ns)", optimized_weights),
        },
                             coords={
                                 "readout_time (ns)":
                                 (("readout_time (ns)", READOUT_PULSE_WIDTH)),
                                 "single_shot_index":
                                 (("single_shot_index",
                                   np.arange(N_SINGLE_SHOTS))),
                             },
                             attrs={
                                 "savgol_window_size":
                                 signal.filter_window_size,
                                 "savgol_order": signal.filter_order,
                                 **asdict(self.log_settings)
                             }).squeeze()

        np.savez(
            f"{self.output_path}/{self.name}/{self.name}_integration_weights",
            cosine=list(optimized_weights.real),
            minus_cosine=list(-1 * optimized_weights.real),
            sine=list(optimized_weights.imag),
            minus_sine=list(-1 * optimized_weights.imag),
        )

        # Plot the data
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8))

        fig.suptitle("\n".join([
            "Readout Integration Weight Optimization",
            f"drive_frequency = {DRIVE_FREQUENCY:.4g} Hz",
            f"drive_amplitude = {drive_powers_in_v:.4f} V",
            f"drive_pulse_width = {DRIVE_PULSE_WIDTH} ns",
            "",
            f"readout_frequency = {READOUT_FREQUENCY:.4g} Hz",
            f"readout_amplitude = {readout_powers_in_v:.4g} V",
            f"readout_pulse_width = {READOUT_PULSE_WIDTH[-1]} ns",
            "",
            f"n_averages: {N_AVERAGES}",
            f"n_single_shots: {N_SINGLE_SHOTS}",
        ]))

        for single_shot in signal.signal.T:
            ax1.scatter(
                READOUT_PULSE_WIDTH,
                single_shot,
                marker=".",
                color="tab:blue",
            )
        ax1.scatter(READOUT_PULSE_WIDTH,
                    signal.average,
                    marker=".",
                    color="tab:orange",
                    label='averaged single_shots')
        ax1.plot(
            READOUT_PULSE_WIDTH,
            signal.smoothed_signal,
            color="tab:green",
            label="smoothed SNR",
        )

        ax1.legend(loc="upper right")

        ax1.set_xlabel("Readout Time [ns]")
        ax1.set_ylabel("||e⟩ - |g⟩| [V]")
        ax1.set_title("signal vs. readout_time")

        ax2.plot(
            READOUT_PULSE_WIDTH,
            optimized_weights,
        )

        ax2.set_xlabel("Readout Time [ns]")
        ax2.set_ylabel("Integration Weight")
        ax2.set_title("integration_weight vs. readout_time")

        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()

        return dataset


if __name__ == "__main__":
    pass
