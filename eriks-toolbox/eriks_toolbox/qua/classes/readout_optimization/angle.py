import os
from copy import deepcopy
from dataclasses import asdict

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from eriks_stlab_toolbox.general.measurement import TwoToneSettings
from eriks_stlab_toolbox.general.utilities.classes import SignalSmoother
from eriks_stlab_toolbox.qua.classes.measurement import QuaMeasurement
from eriks_stlab_toolbox.qua.configuration import TwoToneTuner
from qm.qua import (align, amp, declare, declare_stream, dual_demod, fixed,
                    for_, measure, play, program, save, stream_processing,
                    wait)
from qualang_tools.units import unit

unit_tools = unit(coerce_to_integer=True)


class ReadoutAngleOptimizer(QuaMeasurement):
    measurement_type = "two_tone"

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = TwoToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        tuned_object = TwoToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = TwoToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = TwoToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = TwoToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        if (integration_weights :=
                self.sweep_settings.readout_pulse.path_to_integration_weights):
            tuned_object = TwoToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = TwoToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        tuned_object = TwoToneTuner.set_drive_if_frequency(
            tuned_object, self.sweep_settings.drive_pulse.frequency)
        tuned_object = TwoToneTuner.set_drive_lo_frequency(
            tuned_object, self.sweep_settings.drive_mix.frequency)
        tuned_object = TwoToneTuner.set_drive_if_power(
            tuned_object, self.sweep_settings.drive_pulse.power)
        tuned_object = TwoToneTuner.set_drive_pulse_width(
            tuned_object, self.sweep_settings.drive_pulse.width)
        tuned_object = TwoToneTuner.set_drive_pulse_shape(
            tuned_object, self.sweep_settings.drive_pulse.shape)

        tuned_object = TwoToneTuner.set_n_single_shots(
            tuned_object, self.sweep_settings.sweep.n_single_shots)
        tuned_object = TwoToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = TwoToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = TwoToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        DRIVE_IF_POWER = self.get_settings("sweep.drive_if_power")[0]
        DRIVE_PULSE_SHAPE = self.get_settings("sweep.drive_pulse_shape")
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]

        N_SINGLE_SHOTS = self.get_settings("sweep.n_single_shots")
        N_AVERAGES = self.get_settings("sweep.n_averages")
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        with program() as ro_int_angle_opt:
            m = declare(int)  # QUA variable for the averaging loop
            n = declare(int)  # QUA variable for the averaging loop

            Ig = declare(
                fixed
            )  # QUA variable for the 'I' quadrature when the qubit is in |g>
            Qg = declare(
                fixed
            )  # QUA variable for the 'Q' quadrature when the qubit is in |g>
            Ig_st = declare_stream()
            Qg_st = declare_stream()

            Ie = declare(
                fixed
            )  # QUA variable for the 'I' quadrature when the qubit is in |e>
            Qe = declare(
                fixed
            )  # QUA variable for the 'Q' quadrature when the qubit is in |e>
            Ie_st = declare_stream()
            Qe_st = declare_stream()

            n_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):
                with for_(m, 0, m < N_SINGLE_SHOTS, m + 1):
                    # Measure the ground state.
                    # Measure the state of the resonator
                    measure(
                        amp(DRIVE_IF_POWER) * "readout",
                        "resonator",
                        None,
                        dual_demod.full("cosine", "out1", "sine", "out2", Ig),
                        dual_demod.full("minus_sine", "out1", "cosine", "out2",
                                        Qg),
                    )
                    # Wait for the qubit to decay to the ground state in the case of measurement induced transitions
                    wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                    # Save the 'I' & 'Q' quadratures to their respective streams for the ground state
                    save(Ig, Ig_st)
                    save(Qg, Qg_st)

                    align()  # global align
                    # Play the x180 gate to put the qubit in the excited state
                    play(
                        amp(DRIVE_IF_POWER) * DRIVE_PULSE_SHAPE,
                        "qubit",
                        duration=DRIVE_PULSE_WIDTH * unit_tools.ns,
                    )
                    # Align the two elements to measure after playing the qubit pulse.
                    align("qubit", "resonator")
                    # Measure the state of the resonator
                    measure(
                        amp(READOUT_IF_POWER) * "readout",
                        "resonator",
                        None,
                        dual_demod.full("cosine", "out1", "sine", "out2", Ie),
                        dual_demod.full("minus_sine", "out1", "cosine", "out2",
                                        Qe),
                    )
                    # Wait for the qubit to decay to the ground state
                    wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                    # Save the 'I' & 'Q' quadratures to their respective streams for the excited state
                    save(Ie, Ie_st)
                    save(Qe, Qe_st)

                save(n, n_st)

            with stream_processing():
                n_st.save("iteration")
                # mean values
                Ig_st.buffer(N_SINGLE_SHOTS).average().save("Ig")
                Qg_st.buffer(N_SINGLE_SHOTS).average().save("Qg")
                Ie_st.buffer(N_SINGLE_SHOTS).average().save("Ie")
                Qe_st.buffer(N_SINGLE_SHOTS).average().save("Qe")

        return ro_int_angle_opt

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        DRIVE_FREQUENCY = (self.get_settings("sweep.drive_if_frequency")[0] +
                           self.get_settings("sweep.drive_lo_frequency")[0])
        DRIVE_IF_POWER = self.get_settings("sweep.drive_if_power")[0]
        DRIVE_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.pi_wf.samples"))
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_FREQUENCY = (
            self.get_settings("sweep.readout_if_frequency")[0] +
            self.get_settings("sweep.readout_lo_frequency")[0])
        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]
        READOUT_IF_POWER_SCALAR = self.get_settings(
            "config.waveforms.ro_wf.sample")

        READOUT_PULSE_WIDTH = self.get_settings("sweep.readout_pulse_width")[0]
        READOUT_ANGLE = np.array(self.get_settings("sweep.readout_angle"))

        N_AVERAGES = self.get_settings("sweep.n_averages")
        N_SINGLE_SHOTS = self.get_settings("sweep.n_single_shots")

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        Ig = unit_tools.demod2volts(
            res_handles.get("Ig").fetch_all(),
            READOUT_PULSE_WIDTH)  # transpose to get amplitude over axis 0
        Qg = unit_tools.demod2volts(
            res_handles.get("Qg").fetch_all(), READOUT_PULSE_WIDTH)
        Ie = unit_tools.demod2volts(
            res_handles.get("Ie").fetch_all(), READOUT_PULSE_WIDTH)
        Qe = unit_tools.demod2volts(
            res_handles.get("Qe").fetch_all(), READOUT_PULSE_WIDTH)

        Ig_rotated = np.array([
            Ig * np.cos(READOUT_ANGLE) - Qg * np.sin(READOUT_ANGLE)
            for Ig, Qg in zip(Ig, Qg)
        ]).T
        Qg_rotated = np.array([
            Ig * np.sin(READOUT_ANGLE) + Qg * np.cos(READOUT_ANGLE)
            for Ig, Qg in zip(Ig, Qg)
        ]).T

        Ie_rotated = np.array([
            Ie * np.cos(READOUT_ANGLE) - Qe * np.sin(READOUT_ANGLE)
            for Ie, Qe in zip(Ie, Qe)
        ]).T
        Qe_rotated = np.array([
            Ie * np.sin(READOUT_ANGLE) + Qe * np.cos(READOUT_ANGLE)
            for Ie, Qe in zip(Ie, Qe)
        ]).T

        readout_powers_in_v = READOUT_IF_POWER * READOUT_IF_POWER_SCALAR
        drive_powers_in_v = DRIVE_IF_POWER * round(DRIVE_IF_POWER_SCALAR, 3)

        signal = SignalSmoother(Ie_rotated - Ig_rotated)
        imaginary_signal = SignalSmoother(Qe_rotated - Qg_rotated)
        if any(
                np.isfinite(signal.smoothed_signal**2 /
                            signal.smoothed_variance)):
            snr = signal.smoothed_signal**2 / signal.smoothed_variance
            snr_str = "snr"
        else:
            snr = signal.smoothed_signal
            snr_str = "signal (V)"

        dims = [
            "readout_angle (rad)",
            "single_shot_index",
        ]

        dataset = xr.Dataset(
            data_vars={
                "Ig_rotated (V)": (dims, Ig_rotated),
                "Qg_rotated (V)": (dims, Qg_rotated),
                "Ie_rotated (V)": (dims, Ie_rotated),
                "Qe_rotated (V)": (dims, Qe_rotated),
                "signal (V)": (dims, np.abs(signal.signal)),
                "average_signal (V)": (
                    "readout_pulse_width (ns)",
                    np.abs(signal.average),
                ),
                "smoothed_signal (V)": (
                    "readout_pulse_width (ns)",
                    signal.smoothed_signal,
                ),
                "variance": ("readout_frequency (Hz)", signal.variance),
                "smoothed_variance": (
                    "readout_frequency (Hz)",
                    signal.smoothed_variance,
                ),
            },
            coords={
                "readout_angle (rad)":
                (("readout_angle (rad)", READOUT_ANGLE)),
                "single_shot_index":
                (("single_shot_index", np.arange(N_SINGLE_SHOTS))),
            },
            attrs={
                "optimal_pulse_width": READOUT_ANGLE[np.argmax(snr)],
                "savgol_window_size": signal.filter_window_size,
                "savgol_order": signal.filter_order,
                **asdict(self.log_settings),
            },
        ).squeeze()

        # Plot the data
        fig, ax = plt.subplots(1, 1, figsize=(8, 8))

        fig.suptitle("\n".join([
            "Readout Angle Optimization",
            f"drive_frequency = {DRIVE_FREQUENCY:.4g} Hz",
            f"drive_amplitude = {drive_powers_in_v:.4f} V",
            f"drive_pulse_width = {DRIVE_PULSE_WIDTH} ns",
            "",
            f"readout_frequency = {READOUT_FREQUENCY:.4g} Hz",
            f"readout_amplitude = {readout_powers_in_v:.4g} V",
            f"readout_pulse_width = {READOUT_PULSE_WIDTH} ns",
            "",
            f"n_averages: {N_AVERAGES}",
            f"n_single_shots: {N_SINGLE_SHOTS}",
        ]))
        for single_shot in signal.signal.T:
            ax.scatter(
                READOUT_ANGLE,
                single_shot,
                marker=".",
                color="tab:blue",
            )
        ax.scatter(
            READOUT_ANGLE,
            signal.average,
            marker=".",
            color="tab:orange",
            label="averaged single_shots I",
        )
        ax.scatter(
            READOUT_ANGLE,
            imaginary_signal.average,
            marker=".",
            color="tab:red",
            label="averaged single_shots Q",
        )
        ax.plot(
            READOUT_ANGLE,
            signal.smoothed_signal,
            "-",
            color="tab:green",
            label="\n".join([
                "smoothed, averaged single_shots I",
                f"optimal readout_angle: {READOUT_ANGLE[np.argmax(snr)] / np.pi:.4f} \u03C0 rad",
                f"{snr_str}: {max(snr):.3g}",
            ]),
        )

        ax.set_xlabel("Readout Angle [rad]")
        ax.set_ylabel("|Ie_rotated⟩ - |Ig_rotated⟩ [V]")

        ax.legend(loc="upper right")
        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()

        return dataset


if __name__ == "__main__":
    pass
