import os
from copy import deepcopy
from dataclasses import asdict

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from eriks_stlab_toolbox.general.measurement import MultiToneSettings
from eriks_stlab_toolbox.general.utilities import rgetattr
from eriks_stlab_toolbox.qua.classes.measurement import QuaMeasurement
from eriks_stlab_toolbox.qua.configuration import MultiToneTuner
from qm.qua import (align, amp, declare, declare_stream, for_, measure, play,
                    program, reset_phase, save, stream_processing, wait)
from qualang_tools.units import unit

unit_tools = unit(coerce_to_integer=True)

# TODO: if else tuned_settings / settings -> tuned_settings only


class InputOffsetOptimizer(QuaMeasurement):
    measurement_type = "multi_tone"

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = MultiToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        for i in [1, 2, 3, 4]:
            if rgetattr(self, f'sweep_settings.drive_{i}_pulse') is not None:
                tuned_object = MultiToneTuner.set_drive_if_frequency(
                    tuned_object, i,
                    rgetattr(self,
                             f'sweep_settings.drive_{i}_pulse.frequency'))
                tuned_object = MultiToneTuner.set_drive_lo_frequency(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_mix.frequency'))
                tuned_object = MultiToneTuner.set_drive_if_power(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.power'))
                tuned_object = MultiToneTuner.set_drive_pulse_width(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.width'))
                tuned_object = MultiToneTuner.set_drive_pulse_shape(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.shape'))

        tuned_object = MultiToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = MultiToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = MultiToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = MultiToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        tuned_object = MultiToneTuner.set_readout_angle(tuned_object, 0)

        tuned_object = MultiToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = MultiToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = MultiToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        IF_POWER = self.get_settings("sweep.readout_if_power")[0]

        N_AVERAGES = self.get_settings("sweep.n_averages")
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        ELEMENTS = sorted(list(self.get_settings("config.elements")))

        with program() as raw_trace:
            n = declare(int)

            trace_st = declare_stream(adc_trace=True)
            n_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):

                power = self.get_settings(f"sweep.drive_1_if_power")[0]
                shape = self.get_settings(f"sweep.drive_1_pulse_shape")

                reset_phase("qubit_1")
                play(shape * amp(power), 'qubit_1')

                align('qubit_1', 'resonator')

                reset_phase("resonator")
                measure("readout" * amp(IF_POWER), "resonator", trace_st)

                wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                save(n, n_st)

            with stream_processing():
                n_st.save("iteration")

                # save average
                trace_st.input1().average().save("Input_1_average")
                trace_st.input2().average().save("Input_2_average")

                # save last run
                trace_st.input1().save("Input_1_single")
                trace_st.input2().save("Input_2_single")

        return raw_trace

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        PULSE_WIDTH = self.get_settings("sweep.readout_pulse_width")[0]
        IF_POWER = self.get_settings("sweep.readout_if_power")[0]
        IF_POWER_SCALAR = self.get_settings("config.waveforms.ro_wf.sample")
        FREQUENCY = self.get_settings("sweep.readout_if_frequency")[
            0] + self.get_settings("sweep.readout_lo_frequency")[0]

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        input_1_single = unit_tools.raw2volts(
            res_handles.get("Input_1_single").fetch_all())
        input_2_single = unit_tools.raw2volts(
            res_handles.get("Input_2_single").fetch_all())
        input_1_average = unit_tools.raw2volts(
            res_handles.get("Input_1_average").fetch_all())
        input_2_average = unit_tools.raw2volts(
            res_handles.get("Input_2_average").fetch_all())

        time = np.arange(len(input_1_average))

        powers_in_v = IF_POWER * IF_POWER_SCALAR

        dims = ["time (ns)"]

        dataset = xr.Dataset(
            data_vars={
                "input_1_single [V]": (dims, input_1_single),
                "input_2_single [V]": (dims, input_2_single),
                "input_1_average [V]": (dims, input_1_average),
                "input_2_average [V]": (dims, input_2_average),
            },
            coords={
                "time (ns)": (("time (ns)", time)),
            },
            attrs={
                "input_1_offset [V]": np.average(input_1_average),
                "input_2_offset [V]": np.average(input_2_average),
                **asdict(self.log_settings),
            },
        ).squeeze()

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8), sharey=False)

        fig.suptitle("\n".join([
            "Input Offset Optimization",
            f"readout_frequency = {FREQUENCY:.4g} Hz",
            f"readout_amplitude = {powers_in_v:.4g} V",
            f"readout_pulse_width = {PULSE_WIDTH} ns",
        ]))

        ax1.set_title("Last Run")
        ax1.set_xlabel("Time [ns]")
        ax1.set_ylabel("Signal [V]")
        ax1.plot(input_1_single, label="Input 1")
        ax1.plot(input_2_single, label="Input 2")
        ax1.axhline(y=0, color="k", ls="-", alpha=1)
        ax1.legend(loc="upper right")

        ax2.set_title("Average Run")
        ax2.set_xlabel("Time [ns]")
        ax2.set_ylabel("Signal [V]")
        ax2.plot(input_1_average, label="I")
        ax2.plot(input_2_average, label="Q")
        ax2.axhline(y=0, color="k", ls="-", alpha=1)
        ax2.axhline(
            y=0,
            color="k",
            ls="--",
            alpha=0.5,
            label=f"Input 1 Offset: {np.average(input_1_average):10f} V")
        ax2.axhline(
            y=0,
            color="k",
            ls="--",
            alpha=0.5,
            label=f"Input 2 Offset: {np.average(input_2_average):.10f} V")
        ax2.legend(loc="upper right")

        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()

        return dataset


if __name__ == "__main__":
    pass
