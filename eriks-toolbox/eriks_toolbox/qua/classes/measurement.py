import json
import logging
import os
import shutil
import sys
from abc import abstractmethod
from copy import deepcopy
from traceback import format_exc

from eriks_stlab_toolbox.general.measurement import Log, Measurement
from eriks_stlab_toolbox.general.utilities import (replace_strings, rgetattr,
                                                   rsetattr)

from eriks_stlab_toolbox.qua.configuration import (OctaveUnit,
                                                   octave_declaration)
from octave_sdk import RFOutputMode
from qm import QuantumMachinesManager
from qualang_tools.results import fetching_tool, progress_counter

logger = logging.getLogger('qm')


class QuaMeasurement(Measurement):

    measurement_type = "single_tone"

    def __init__(
            self,
            instrument_config: dict,
            log_settings: Log,
            output_path: str = os.path.dirname(__file__),
    ):
        """base class for running measurements using octave/opx and qua language
        Args:
            instrument_config (dict): dictionary containing octave & opx connectivity configuration
                (see measurement scripts for reference)
            log_settings (dict): either Log object or dictionary for making Log object
            output_path (str): path to save data, metadata, script and settings in
        """

        self.instrument_config = instrument_config

        super().__init__(log_settings, output_path=output_path)

    def make_settings(self):
        path = os.path.dirname(os.path.dirname(__file__))

        # load default settings for measurement type (e.g. singletone, twotone)
        try:
            with open(
                    f"{path}/configuration/{self.measurement_type}/settings.json",
                    encoding="utf-8",
            ) as settings:
                default_settings = json.load(settings)
        except NameError:
            logger.warning(
                f"no default settings found at {path}/settings.json")
            default_settings = {}

        # check for opx settings
        try:
            opx_settings = self.instrument_config["opx"]
        except KeyError:
            logger.error("Please define 'opx' in instrument_settings")
            raise

        # check for opx analog output settings
        try:
            analog_outputs = opx_settings["analog_outputs"]
        except KeyError:
            logger.warning(
                "Please define 'analog_outputs' in instrument_settings['opx']")
        else:
            rsetattr(
                default_settings,
                "config.controllers.con1.analog_outputs",
                analog_outputs,
            )

        # check for opx analog input settings
        try:
            analog_inputs = opx_settings["analog_inputs"]
        except KeyError:
            logger.warning('\n'.join([
                "No 'analog_inputs' found in instrument_settings['opx']",
                "Default settings are used - {'1': {'offset': 0, 'gain_db': 0}, '2': {'offset': 0, 'gain_db': 0}}"
            ]))
        else:
            rsetattr(default_settings, "config.controllers.con1.analog_inputs",
                     analog_inputs)

        # check for op communication settings
        try:
            opx_communication = opx_settings.pop("communication")
        except KeyError:
            logger.warning('\n'.join([
                "No 'communication' found in instrument_settings['opx']",
                "Default settings are used - {'ip': '172.19.20.29', 'port': 9510}"
            ]))
        else:
            rsetattr(default_settings, "communication.opx", opx_communication)

        # check for octave settings
        try:
            octave_settings_ = self.instrument_config["octave"]
        except KeyError:
            logger.error("Please define 'octave' in instrument_settings")
            raise

        # check for octave communication settings
        try:
            octave_communication = octave_settings_.pop("communication")
        except KeyError:
            logger.warning('\n'.join([
                "No 'communication' found in instrument_settings['octave']",
                'Default settings are used - {"ip": "172.19.20.31", "port": 80}'
            ]))
        else:
            rsetattr(default_settings, "communication.octave",
                     octave_communication)

        # check for calibration settings
        try:
            calibrate = octave_settings_.pop("calibrate")
        except KeyError:
            logger.warning('\n'.join([
                "No 'calibrate' found in instrument_settings['octave']",
                'Default settings are used - {"calibrate": True}'
            ]))
        else:
            rsetattr(default_settings, "calibrate", calibrate)

        # set octave mixer settings (mixer name + I/Q ports used)
        default_settings_copy = deepcopy(default_settings)
        ports_used = {
            element_settings['port']: element_settings.get("gain", 0)
            for element_settings in octave_settings_.values()
        }
        for port in default_settings['config']['octaves']['octave1'][
                'RF_outputs'].keys():
            if ports_used.get(int(port)) is None:
                default_settings_copy['config']['octaves']['octave1'][
                    'RF_outputs'].pop(str(port), None)
            else:
                default_settings_copy['config']['octaves']['octave1'][
                    'RF_outputs'][str(port)]["gain"] = ports_used[int(port)]

        for element, element_settings in default_settings["config"][
                "elements"].items():
            try:
                settings = octave_settings_[element]
            except KeyError:
                logger.info(
                    f'Skipping element "{element}", because it is not defined in octave_settings'
                )
                default_settings_copy["config"]["elements"].pop(element, None)

                waveforms = [
                    wf
                    for wf in default_settings["config"]["waveforms"].keys()
                    if wf.startswith(element)
                ]
                for wf in waveforms:
                    default_settings_copy["config"]["waveforms"].pop(wf, None)

                pulses = [
                    p for p in default_settings["config"]["pulses"].keys()
                    if p.startswith(element)
                ]
                for p in pulses:
                    default_settings_copy["config"]["pulses"].pop(p, None)

                continue

            try:
                element_settings = replace_strings(element_settings, "<port>",
                                                   settings["port"])
            except KeyError:
                print(f'Please define port in octave_settings[{element}]')
                raise

            default_settings_copy["config"]["elements"][
                element] = element_settings

        return default_settings_copy

    @abstractmethod
    def tune_settings(self):
        ...

    def save_settings(self):
        path = f"{self.output_path}/{self.name}"

        if not os.path.isdir(path):
            os.makedirs(path)
            print(f"created folder: '{self.name}' with path '{path}'")

        with open(f"{path}/{self.name}_settings.json", "w") as new_file:
            json.dump(self.tuned_settings, new_file)

        shutil.copy(
            sys.argv[0],
            f"{path}/{self.name}_script.py",
        )

        return path

    @abstractmethod
    def make_measurement(self):
        ...

    def do_measurement(self):
        opx_ip, opx_port = self.opx_address
        octave_ip, octave_port = self.octave_address

        octave_config = octave_declaration(
            [OctaveUnit("octave1", octave_ip, octave_port, con="con1")])

        quantum_machines_manager = QuantumMachinesManager(host=opx_ip,
                                                          port=opx_port,
                                                          octave=octave_config)

        try:

            quantum_machine = quantum_machines_manager.open_qm(
                self.tuned_settings["config"])

            if self.tuned_settings["calibrate"]:
                for element in sorted(
                        list(self.get_settings("config.elements"))):

                    quantum_machine.calibrate_element(element)
                    logger.info(f'Calibrating element "{element}"')

            job = quantum_machine.execute(self.measurement_program)

            results = fetching_tool(job, data_list=["iteration"], mode="live")

            temp_iteration = 0
            while results.is_processing():

                (iteration, ) = results.fetch_all()

                if iteration != temp_iteration:
                    progress_counter(
                        iteration,
                        self.get_settings("sweep.n_averages"),
                        progress_bar=True,
                        percent=True,
                        start_time=results.get_start_time(),
                    )
                else:
                    temp_iteration = iteration

            job.execution_report()

            return job

        except KeyboardInterrupt:
            print("program interupted")
            exit()

        except Exception as err:
            print(f"Unexpected {err=}, {type(err)=}")
            print(format_exc())
            exit()

        finally:
            print('closing instruments')
            for element in sorted(list(self.get_settings("config.elements"))):
                quantum_machine.octave.set_rf_output_mode(
                    element, RFOutputMode.off)

            quantum_machine.close()
            quantum_machines_manager.close()

    @abstractmethod
    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        ...

    @property
    def opx_address(self):
        """
        Returns:
            opx_ip (str): opx IP address
            opx_port (int): opx port
        """
        opx_ip = rgetattr(self.tuned_settings, "communication.opx.ip")
        opx_port = rgetattr(self.tuned_settings, "communication.opx.port")

        return opx_ip, opx_port

    @property
    def octave_address(self):
        """
        Returns:
            octave_ip (str): octave IP address
            octave_port (int): octave port
        """
        octave_ip = rgetattr(self.tuned_settings, "communication.octave.ip")
        octave_port = rgetattr(self.tuned_settings,
                               "communication.octave.port")

        return octave_ip, octave_port

    def get_settings(self, dir_: str):
        """helper function for getting a value from self.settings in nested way.
        For example, self.get_settings("A.B.C.D") would return self.settings['A']['B']['C']['D']
        Args:
            dir_ (str): directory to retrieve value from
        Returns:
            value: value at input directory
        """
        if getattr(self, "tuned_settings", None):
            settings = self.tuned_settings
        else:
            settings = self.settings

        names = dir_.split(".")

        try:
            value = rgetattr(settings, dir_)
        except ValueError as e:
            print(
                f"{e}: {names[-1]} not defined at [{']['.join(names)}] in self.tuned_settings"
            )
            raise

        return value

    def set_settings(self, dir_: str, value):
        """helper function for setting a value in self.settings in nested way.
        For example, self.set_settings("A.B.C.D") would set self.settings['A']['B']['C']['D']
        Args:
            dir_ (str): directory to put value in
            value: value to be put in
        """
        if getattr(self, "tuned_settings", None):
            settings = self.tuned_settings
        else:
            settings = self.settings

        names = dir_.split(".")

        try:
            rgetattr(settings, dir_)
        except ValueError as e:
            print(
                f"{e}: {names[-1]} not defined at [{']['.join(names)}] in self.tuned_settings"
            )
            print("-> directory is created")
        finally:
            rsetattr(settings, dir_, value)


if __name__ == "__main__":
    pass
