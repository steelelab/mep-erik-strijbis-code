import os
from copy import deepcopy
from dataclasses import asdict

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from qm.qua import (amp, declare, declare_stream, dual_demod, fixed, for_,
                    measure, program, save, stream_processing,
                    update_frequency, wait)
from qualang_tools.loops import from_array
from qualang_tools.units import unit

from eriks_stlab_toolbox.general.measurement import SingleToneSettings
from eriks_stlab_toolbox.general.utilities import v_to_dbm, x_to_db
from eriks_stlab_toolbox.qua.classes.measurement import QuaMeasurement
from eriks_stlab_toolbox.qua.configuration import SingleToneTuner

# from eriks_stlab_toolbox.measurement.fitting import FanoFitter,

unit_tools = unit(coerce_to_integer=True)

# TODO: if else tuned_settings / settings -> tuned_settings only


class ResonatorSpectroscopy(QuaMeasurement):
    settings_tuner = "single_tone"
    """class for sweeping resonator_f vs. resonator_amp
    sweep_settings should be in SingleToneSettings format
    """

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = SingleToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        tuned_object = SingleToneTuner.set_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = SingleToneTuner.set_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = SingleToneTuner.set_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)

        if integration_weights := self.sweep_settings.readout_pulse.path_to_integration_weights:
            tuned_object = SingleToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = SingleToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        tuned_object = SingleToneTuner.set_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        tuned_object = SingleToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = SingleToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = SingleToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        IF_FREQUENCY = np.array(self.get_settings("sweep.if_frequency"))
        IF_POWER = np.array(self.get_settings("sweep.if_power"))

        N_AVERAGES = self.get_settings("sweep.n_averages")
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        with program() as resonator_spectroscopy:
            n = declare(int)
            f = declare(int)
            a = declare(fixed)

            I = declare(fixed)
            Q = declare(fixed)

            I_st = declare_stream()
            Q_st = declare_stream()
            n_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):  # averaging
                with for_(*from_array(f, IF_FREQUENCY)):
                    update_frequency("resonator", f)

                    with for_(*from_array(a, IF_POWER)):
                        measure(
                            "readout" * amp(a),
                            "resonator",
                            None,
                            dual_demod.full("cosine", "out1", "sine", "out2",
                                            I),
                            dual_demod.full("minus_sine", "out1", "cosine",
                                            "out2", Q),
                        )
                        wait(WAIT_BETWEEN * unit_tools.ns, "resonator")
                        save(I, I_st)
                        save(Q, Q_st)
                save(n, n_st)

            with stream_processing():
                I_st.buffer(len(IF_POWER)).buffer(
                    len(IF_FREQUENCY)).average().save("I")
                Q_st.buffer(len(IF_POWER)).buffer(
                    len(IF_FREQUENCY)).average().save("Q")
                n_st.save("iteration")

        return resonator_spectroscopy

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        PULSE_WIDTH = self.get_settings("sweep.pulse_width")[0]
        IF_POWER = np.array(self.get_settings("sweep.if_power"))
        IF_POWER_SCALAR = self.get_settings("config.waveforms.ro_wf.sample")
        FREQUENCY = np.array(
            self.get_settings("sweep.if_frequency")) + np.array(
                self.get_settings("sweep.lo_frequency"))

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        i = unit_tools.demod2volts(
            res_handles.get("I").fetch_all(), PULSE_WIDTH)
        q = unit_tools.demod2volts(
            res_handles.get("Q").fetch_all(), PULSE_WIDTH)

        complex_signal = i + 1j * q

        powers_in_v = IF_POWER * IF_POWER_SCALAR
        powers_in_dbm = v_to_dbm(powers_in_v)

        s21 = complex_signal / powers_in_v

        phase = np.angle(complex_signal)

        if len(FREQUENCY) > 1:
            unwrapped_phase = np.unwrap(phase, axis=0)
            group_delay = -1 * np.gradient(
                unwrapped_phase, 2 * np.pi * np.diff(FREQUENCY)[0], axis=0)

        else:
            unwrapped_phase = phase
            group_delay = np.zeros(phase.shape)

        dims = [
            "frequency (Hz)",
            "amplitude (dBm)",
        ]

        dataset = xr.Dataset(
            data_vars={
                "s21_real [_]": (dims, s21.real),
                "s21_imaginary [_]": (dims, s21.imag),
                "phase [rad]": (dims, phase),
                "s21_magnitude [dB]": (dims, x_to_db(np.abs(s21))),
                "unwrapped_phase [rad]": (dims, unwrapped_phase),
                "group_delay [s]": (dims, group_delay),
                "real [V]": (dims, i),
                "imaginary [V]": (dims, q),
                "magnitude [V]": (dims, np.abs(complex_signal)),
            },
            coords={
                "frequency (Hz)": (("frequency (Hz)", FREQUENCY)),
                "amplitude (dBm)": (("amplitude (dBm)", powers_in_dbm)),
            },
            attrs=asdict(self.log_settings),
        ).squeeze()

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8))

        fig.suptitle(f"Resonator Spectroscopy, pulse_width = {PULSE_WIDTH} ns")

        ax1.set_title("frequency vs. amplitude")
        X, Y = np.meshgrid(FREQUENCY, powers_in_dbm)
        im = ax1.pcolor(X, Y, x_to_db(np.abs(s21.T)), cmap="RdBu_r")

        cbar = plt.colorbar(im, ax=ax1)
        cbar.ax.get_yaxis().labelpad = 15
        cbar.ax.set_ylabel("S21 [dB]", rotation=270)

        ax1.set_xlabel("Frequency [Hz]")
        ax1.set_ylabel("Amplitude [dBm]")

        ax2.set_title(f"linecut @ amplitude = {powers_in_dbm[0]:.0f} dBm")
        ax2.plot(FREQUENCY, x_to_db(np.abs(s21.T))[0])

        ax2.set_xlabel("Frequency [Hz]")
        ax2.set_ylabel("S21 [dB]")

        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()


if __name__ == "__main__":
    pass
