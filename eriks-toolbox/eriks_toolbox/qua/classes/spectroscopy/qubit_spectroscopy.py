import os
from copy import deepcopy
from dataclasses import asdict

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from eriks_stlab_toolbox.general.measurement import TwoToneSettings
from eriks_stlab_toolbox.general.utilities import v_to_dbm, x_to_db
from qm.qua import (align, amp, declare, declare_stream, dual_demod, fixed,
                    for_, measure, play, program, save, stream_processing,
                    update_frequency, wait)
from eriks_stlab_toolbox.qua.classes.measurement import QuaMeasurement
from eriks_stlab_toolbox.qua.configuration import TwoToneTuner
from qualang_tools.loops import from_array
from qualang_tools.units import unit

unit_tools = unit(coerce_to_integer=True)

# TODO: if else tuned_settings / settings -> tuned_settings only


class QubitSpectroscopy(QuaMeasurement):
    measurement_type = "two_tone"
    """class for sweeping resonator_f vs. resonator_amp vs. qubit_f vs. qubit_amp
    sweep_settings should be in TwoToneSettings format
    """

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = TwoToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        tuned_object = TwoToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = TwoToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = TwoToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = TwoToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        if integration_weights := self.sweep_settings.readout_pulse.path_to_integration_weights:
            tuned_object = TwoToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = TwoToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        tuned_object = TwoToneTuner.set_drive_if_frequency(
            tuned_object, self.sweep_settings.drive_pulse.frequency)
        tuned_object = TwoToneTuner.set_drive_lo_frequency(
            tuned_object, self.sweep_settings.drive_mix.frequency)
        tuned_object = TwoToneTuner.set_drive_if_power(
            tuned_object, self.sweep_settings.drive_pulse.power)
        tuned_object = TwoToneTuner.set_drive_pulse_width(
            tuned_object, self.sweep_settings.drive_pulse.width)
        tuned_object = TwoToneTuner.set_drive_pulse_shape(
            tuned_object, self.sweep_settings.drive_pulse.shape)

        tuned_object = TwoToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = TwoToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = TwoToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)
        tuned_object = TwoToneTuner.set_overlapping_readout(
            tuned_object, self.sweep_settings.sweep.overlapping_readout)

        return tuned_object.settings

    def make_measurement(self):
        DRIVE_IF_FREQUENCY = np.array(
            self.get_settings("sweep.drive_if_frequency"))
        DRIVE_IF_POWER = np.array(self.get_settings("sweep.drive_if_power"))
        DRIVE_PULSE_SHAPE = self.get_settings("sweep.drive_pulse_shape")
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_IF_FREQUENCY = np.array(
            self.get_settings("sweep.readout_if_frequency"))
        READOUT_IF_POWER = np.array(
            self.get_settings("sweep.readout_if_power"))
        READOUT_PULSE_WIDTH = self.get_settings("sweep.readout_pulse_width")[0]

        N_AVERAGES = self.get_settings("sweep.n_averages")
        OVERLAPPING_READOUT = self.get_settings("sweep.overlapping_readout")
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        with program() as qubit_spectroscopy:
            n = declare(int)  # averaging
            f_readout = declare(int)  # resonator frequencies
            f_drive = declare(int)  # qubit frequencies
            a_readout = declare(fixed)  # resonator pulse amplitudes
            a_drive = declare(fixed)  # qubit pulse amplitudes

            I = declare(fixed)
            Q = declare(fixed)

            I_st = declare_stream()
            Q_st = declare_stream()
            n_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):
                with for_(*from_array(f_readout, READOUT_IF_FREQUENCY)):
                    update_frequency("resonator", f_readout)
                    with for_(*from_array(f_drive, DRIVE_IF_FREQUENCY)):
                        update_frequency("qubit", f_drive)

                        with for_(*from_array(a_readout, READOUT_IF_POWER)):
                            with for_(*from_array(a_drive, DRIVE_IF_POWER)):
                                play(DRIVE_PULSE_SHAPE * amp(a_drive),
                                     "qubit",
                                     duration=DRIVE_PULSE_WIDTH *
                                     unit_tools.ns)

                                if OVERLAPPING_READOUT:
                                    wait(
                                        int(DRIVE_PULSE_WIDTH -
                                            READOUT_PULSE_WIDTH) *
                                        unit_tools.ns,
                                        "qubit",
                                    )
                                align("qubit", "resonator")

                                measure(
                                    "readout" * amp(a_readout),
                                    "resonator",
                                    None,
                                    dual_demod.full("cosine", "out1", "sine",
                                                    "out2", I),
                                    dual_demod.full("minus_sine", "out1",
                                                    "cosine", "out2", Q),
                                )

                                wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                                save(I, I_st)
                                save(Q, Q_st)
                save(n, n_st)

            with stream_processing():
                I_st.buffer(
                    len(DRIVE_IF_POWER)).buffer(len(READOUT_IF_POWER)).buffer(
                        len(DRIVE_IF_FREQUENCY)).buffer(
                            len(READOUT_IF_FREQUENCY)).average().save("I")
                Q_st.buffer(
                    len(DRIVE_IF_POWER)).buffer(len(READOUT_IF_POWER)).buffer(
                        len(DRIVE_IF_FREQUENCY)).buffer(
                            len(READOUT_IF_FREQUENCY)).average().save("Q")
                n_st.save("iteration")

        return qubit_spectroscopy

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]
        DRIVE_IF_POWER = np.array(self.get_settings("sweep.drive_if_power"))
        DRIVE_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.pi_wf.samples"))
        DRIVE_FREQUENCY = np.array(
            self.get_settings("sweep.drive_if_frequency")) + np.array(
                self.get_settings("sweep.drive_lo_frequency"))

        READOUT_PULSE_WIDTH = self.get_settings("sweep.readout_pulse_width")[0]
        READOUT_IF_POWER = np.array(
            self.get_settings("sweep.readout_if_power"))
        READOUT_IF_POWER_SCALAR = self.get_settings(
            "config.waveforms.ro_wf.sample")
        READOUT_FREQUENCY = np.array(
            self.get_settings("sweep.readout_if_frequency")) + np.array(
                self.get_settings("sweep.readout_lo_frequency"))

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        i = unit_tools.demod2volts(
            res_handles.get("I").fetch_all(), READOUT_PULSE_WIDTH)
        q = unit_tools.demod2volts(
            res_handles.get("Q").fetch_all(), READOUT_PULSE_WIDTH)

        complex_signal = i + 1j * q
        readout_powers_in_v = READOUT_IF_POWER * READOUT_IF_POWER_SCALAR
        readout_powers_in_dbm = v_to_dbm(readout_powers_in_v)
        drive_powers_in_v = DRIVE_IF_POWER * round(DRIVE_IF_POWER_SCALAR, 3)
        drive_powers_in_dbm = v_to_dbm(drive_powers_in_v)

        s21 = complex_signal / readout_powers_in_v[:, np.
                                                   newaxis]  # add axis to divide along readout power axis

        phase = np.angle(complex_signal)

        if len(READOUT_FREQUENCY) > 1:
            unwrapped_phase = np.unwrap(phase, axis=0)
            group_delay = -1 * np.gradient(
                unwrapped_phase,
                2 * np.pi * np.diff(READOUT_FREQUENCY)[0],
                axis=0)

        else:
            unwrapped_phase = phase
            group_delay = np.zeros(phase.shape)

        dims = [
            "readout_frequency (Hz)",
            "drive_frequency (Hz)",
            "readout_amplitude (dBm)",
            "drive_amplitude (dBm)",
        ]

        dataset = xr.Dataset(
            data_vars={
                "s21_real [_]": (dims, s21.real),
                "s21_imaginary [_]": (dims, s21.imag),
                "phase [rad]": (dims, phase),
                "s21_magnitude [dB]": (dims, x_to_db(np.abs(s21))),
                "unwrapped_phase [rad]": (dims, unwrapped_phase),
                "group_delay [s]": (dims, group_delay),
                "real [V]": (dims, i),
                "imaginary [V]": (dims, q),
                "magnitude [V]": (dims, np.abs(complex_signal)),
            },
            coords={
                "readout_frequency (Hz)":
                (("readout_frequency (Hz)", READOUT_FREQUENCY)),
                "drive_frequency (Hz)":
                (("drive_frequency (Hz)", DRIVE_FREQUENCY)),
                "readout_amplitude (dBm)":
                (("readout_amplitude (dBm)", readout_powers_in_dbm)),
                "drive_amplitude (dBm)":
                (("drive_amplitude (dBm)", drive_powers_in_dbm)),
            },
            attrs=asdict(self.log_settings),
        ).squeeze()

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8))

        fig.suptitle("\n".join([
            "Qubit Spectroscopy",
            f"drive_pulse_width = {DRIVE_PULSE_WIDTH} ns",
            f"readout pulse width = {READOUT_PULSE_WIDTH} ns",
        ]))

        ax1.set_title("\n".join([
            "drive_frequency vs. drive_amplitude",
            f"@ readout_frequency = {READOUT_FREQUENCY[-1]:.3g} Hz",
            f"@ readout_amplitude = {readout_powers_in_dbm[-1]:.1f} dBm",
        ]))
        X, Y = np.meshgrid(DRIVE_FREQUENCY, drive_powers_in_dbm)
        im = ax1.pcolor(X,
                        Y,
                        x_to_db(np.abs(s21[-1, :, -1, :].T)),
                        cmap="RdBu_r")

        cbar = plt.colorbar(im, ax=ax1)
        cbar.ax.get_yaxis().labelpad = 15
        cbar.ax.set_ylabel("S21 [dB]", rotation=270)

        ax1.set_xlabel("Drive Frequency [Hz]")
        ax1.set_ylabel("Drive Amplitude [dBm]")

        ax2.set_title(
            f"linecut @ drive_amplitude = {drive_powers_in_dbm[-1]:.0f} dBm")
        ax2.plot(DRIVE_FREQUENCY, x_to_db(np.abs(s21[-1, :, -1, :].T))[-1])

        ax2.set_xlabel("Drive Frequency [Hz]")
        ax2.set_ylabel("S21 [dB]")

        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()

        return dataset


if __name__ == "__main__":
    pass
