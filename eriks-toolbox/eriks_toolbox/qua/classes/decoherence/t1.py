# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"></ul></div>

import os
from copy import deepcopy
from dataclasses import asdict

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from eriks_stlab_toolbox.general.measurement import (MultiToneSettings,
                                                     TwoToneSettings)
from eriks_stlab_toolbox.general.utilities import rgetattr, x_to_db
from eriks_stlab_toolbox.qua.classes.measurement import QuaMeasurement
from eriks_stlab_toolbox.qua.configuration import MultiToneTuner, TwoToneTuner
from qm.qua import (align, amp, declare, declare_stream, dual_demod, fixed,
                    for_, if_, measure, play, program, save, stream_processing,
                    wait)
from qualang_tools.loops import from_array
from qualang_tools.plot.fitting import Fit
from qualang_tools.units import unit

unit_tools = unit(coerce_to_integer=True)

# TODO: if else tuned_settings / settings -> tuned_settings only


class T1(QuaMeasurement):
    measurement_type = "two_tone"

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = TwoToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        tuned_object = TwoToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = TwoToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = TwoToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = TwoToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        if integration_weights := self.sweep_settings.readout_pulse.path_to_integration_weights:
            tuned_object = TwoToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = TwoToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        tuned_object = TwoToneTuner.set_drive_if_frequency(
            tuned_object, self.sweep_settings.drive_pulse.frequency)
        tuned_object = TwoToneTuner.set_drive_lo_frequency(
            tuned_object, self.sweep_settings.drive_mix.frequency)
        tuned_object = TwoToneTuner.set_drive_if_power(
            tuned_object, self.sweep_settings.drive_pulse.power)
        tuned_object = TwoToneTuner.set_drive_pulse_width(
            tuned_object, self.sweep_settings.drive_pulse.width)
        tuned_object = TwoToneTuner.set_drive_pulse_shape(
            tuned_object, self.sweep_settings.drive_pulse.shape)

        tuned_object = TwoToneTuner.set_idle_time(
            tuned_object, self.sweep_settings.sweep.idle_time)
        tuned_object = TwoToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = TwoToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = TwoToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        DRIVE_IF_POWER = self.get_settings("sweep.drive_if_power")[0]
        DRIVE_PULSE_SHAPE = self.get_settings("sweep.drive_pulse_shape")
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]

        IDLE_TIME = np.array(self.get_settings("sweep.idle_time"))
        N_AVERAGES = self.get_settings("sweep.n_averages")
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        with program() as t1:
            n = declare(int)  # averaging
            t = declare(int)  # qubit pulse amplitudes

            I = declare(fixed)
            Q = declare(fixed)

            I_st = declare_stream()
            Q_st = declare_stream()
            n_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):
                with for_(*from_array(t, IDLE_TIME * unit_tools.ns)):
                    play(
                        DRIVE_PULSE_SHAPE * amp(DRIVE_IF_POWER),
                        "qubit",
                        duration=DRIVE_PULSE_WIDTH * unit_tools.ns,
                    )

                    with if_(t > 0):
                        wait(t, "qubit")

                    align("qubit", "resonator")

                    measure(
                        "readout" * amp(READOUT_IF_POWER),
                        "resonator",
                        None,
                        dual_demod.full("cosine", "out1", "sine", "out2", I),
                        dual_demod.full("minus_sine", "out1", "cosine", "out2",
                                        Q),
                    )

                    wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                    save(I, I_st)
                    save(Q, Q_st)
                save(n, n_st)

            with stream_processing():
                I_st.buffer(len(IDLE_TIME)).average().save("I")
                Q_st.buffer(len(IDLE_TIME)).average().save("Q")
                n_st.save("iteration")

        return t1

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        DRIVE_FREQUENCY = self.get_settings("sweep.drive_if_frequency")[
            0] + self.get_settings("sweep.drive_lo_frequency")[0]
        DRIVE_IF_POWER = self.get_settings("sweep.drive_if_power")[0]
        DRIVE_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.pi_wf.samples"))
        DRIVE_PULSE_WIDTH = self.get_settings("sweep.drive_pulse_width")[0]

        READOUT_FREQUENCY = self.get_settings("sweep.readout_if_frequency")[
            0] + self.get_settings("sweep.readout_lo_frequency")[0]
        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]
        READOUT_IF_POWER_SCALAR = self.get_settings(
            "config.waveforms.ro_wf.sample")
        READOUT_PULSE_WIDTH = self.get_settings("sweep.readout_pulse_width")[0]

        IDLE_TIME = np.array(self.get_settings("sweep.idle_time"))

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        i = unit_tools.demod2volts(
            res_handles.get("I").fetch_all(), READOUT_PULSE_WIDTH)
        q = unit_tools.demod2volts(
            res_handles.get("Q").fetch_all(), READOUT_PULSE_WIDTH)

        complex_signal = i + 1j * q
        readout_powers_in_v = READOUT_IF_POWER * READOUT_IF_POWER_SCALAR

        drive_powers_in_v = DRIVE_IF_POWER * round(DRIVE_IF_POWER_SCALAR, 3)

        s21 = complex_signal / readout_powers_in_v  # add axis to divide along readout power axis

        phase = np.angle(complex_signal)

        t1_fit = Fit().T1(IDLE_TIME, np.abs(complex_signal), plot=False)
        # t1_fit = Fit().T1(IDLE_TIME, complex_signal.real, plot=False)

        dims = [
            "idle_time (ns)",
        ]

        dataset = xr.Dataset(
            data_vars={
                "real [V]": (dims, i),
                "imaginary [V]": (dims, q),
                "magnitude [V]": (dims, np.abs(complex_signal)),
                "fitted_magnitude [V]": (dims, t1_fit["fit_func"](IDLE_TIME)),
                "phase [rad]": (dims, phase),
                "s21_real [_]": (dims, s21.real),
                "s21_imaginary [_]": (dims, s21.imag),
                "s21_magnitude [dB]": (dims, x_to_db(np.abs(s21))),
            },
            coords={
                "idle_time (ns)": (("idle_time (ns)", IDLE_TIME)),
            },
            attrs={
                "T1": np.abs(t1_fit["T1"][0]),
                **asdict(self.log_settings)
            },
        ).squeeze()

        fig, ax1 = plt.subplots(1, 1, figsize=(8, 8))

        fig.suptitle("\n".join([
            "T1",
            f"drive_frequency = {DRIVE_FREQUENCY:.4g} Hz",
            f"drive_amplitude = {drive_powers_in_v:.4f} V",
            f"drive_pulse_width = {DRIVE_PULSE_WIDTH} ns",
            "",
            f"readout_frequency = {READOUT_FREQUENCY:.4g} Hz",
            f"readout_amplitude = {readout_powers_in_v:.4f} V",
            f"readout_pulse_width = {READOUT_PULSE_WIDTH} ns",
        ]))

        ax1.scatter(IDLE_TIME, np.abs(complex_signal), marker=".")
        # ax1.scatter(IDLE_TIME, complex_signal.real, marker=".")

        ax1.plot(
            IDLE_TIME,
            t1_fit["fit_func"](IDLE_TIME),
            color="tab:orange",
            label=f'T1: {np.abs(t1_fit["T1"][0]):.2f} ns',
        )

        ax1.legend(loc="upper right")

        ax1.set_xlabel("Idle Time [ns]")
        ax1.set_ylabel("|I + iQ| [V]")

        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()

        return dataset


class T1_2(QuaMeasurement):
    measurement_type = "multi_tone"

    def __init__(
            self,
            sweep_settings,
            instrument_settings,
            log_settings,
            output_path: str = os.path.dirname(__file__),
    ):
        self.sweep_settings = MultiToneSettings.from_dict(sweep_settings)

        super().__init__(instrument_settings, log_settings, output_path)

    def tune_settings(self):
        tuned_object = deepcopy(self)

        tuned_object = MultiToneTuner.set_readout_if_frequency(
            tuned_object, self.sweep_settings.readout_pulse.frequency)
        tuned_object = MultiToneTuner.set_readout_lo_frequency(
            tuned_object, self.sweep_settings.readout_mix.frequency)
        tuned_object = MultiToneTuner.set_readout_if_power(
            tuned_object, self.sweep_settings.readout_pulse.power)
        tuned_object = MultiToneTuner.set_readout_pulse_width(
            tuned_object, self.sweep_settings.readout_pulse.width)

        if integration_weights := self.sweep_settings.readout_pulse.path_to_integration_weights:
            tuned_object = MultiToneTuner.set_readout_integration_weights(
                tuned_object, integration_weights)
        else:
            tuned_object = MultiToneTuner.set_readout_angle(
                tuned_object, self.sweep_settings.readout_pulse.angle)

        for i in [1, 2, 3, 4]:
            if rgetattr(self, f'sweep_settings.drive_{i}_pulse') is not None:

                tuned_object = MultiToneTuner.set_drive_if_frequency(
                    tuned_object, i,
                    rgetattr(self,
                             f'sweep_settings.drive_{i}_pulse.frequency'))
                tuned_object = MultiToneTuner.set_drive_lo_frequency(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_mix.frequency'))
                tuned_object = MultiToneTuner.set_drive_if_power(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.power'))
                tuned_object = MultiToneTuner.set_drive_pulse_width(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.width'))
                tuned_object = MultiToneTuner.set_drive_pulse_shape(
                    tuned_object, i,
                    rgetattr(self, f'sweep_settings.drive_{i}_pulse.shape'))

        tuned_object = MultiToneTuner.set_idle_time(
            tuned_object, self.sweep_settings.sweep.idle_time)
        tuned_object = MultiToneTuner.set_n_averages(
            tuned_object, self.sweep_settings.sweep.n_averages)
        tuned_object = MultiToneTuner.set_time_of_flight(
            tuned_object, self.sweep_settings.sweep.time_of_flight)
        tuned_object = MultiToneTuner.set_wait_between(
            tuned_object, self.sweep_settings.sweep.wait_between)

        return tuned_object.settings

    def make_measurement(self):
        DRIVE_01_IF_POWER = self.get_settings("sweep.drive_1_if_power")[0]
        DRIVE_01_PULSE_SHAPE = self.get_settings("sweep.drive_1_pulse_shape")
        DRIVE_01_PULSE_WIDTH = self.get_settings(
            "sweep.drive_1_pulse_width")[0]

        DRIVE_12_IF_POWER = self.get_settings("sweep.drive_3_if_power")[0]
        DRIVE_12_PULSE_SHAPE = self.get_settings("sweep.drive_3_pulse_shape")
        DRIVE_12_PULSE_WIDTH = self.get_settings(
            "sweep.drive_3_pulse_width")[0]

        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]

        IDLE_TIME = np.array(self.get_settings("sweep.idle_time"))
        N_AVERAGES = self.get_settings("sweep.n_averages")
        WAIT_BETWEEN = self.get_settings("sweep.wait_between")

        with program() as t1:
            n = declare(int)  # averaging
            t = declare(int)  # qubit pulse amplitudes

            I = declare(fixed)
            Q = declare(fixed)

            I_st = declare_stream()
            Q_st = declare_stream()
            n_st = declare_stream()

            with for_(n, 0, n < N_AVERAGES, n + 1):
                with for_(*from_array(t, IDLE_TIME * unit_tools.ns)):
                    play(
                        DRIVE_01_PULSE_SHAPE * amp(DRIVE_01_IF_POWER),
                        "qubit_1",
                        duration=DRIVE_01_PULSE_WIDTH * unit_tools.ns,
                    )

                    align('qubit_1', 'qubit_3')

                    play(
                        DRIVE_12_PULSE_SHAPE * amp(DRIVE_12_IF_POWER),
                        "qubit_3",
                        duration=DRIVE_12_PULSE_WIDTH * unit_tools.ns,
                    )

                    with if_(t > 0):
                        wait(t, "qubit_3")

                    align("qubit_3", "resonator")

                    measure(
                        "readout" * amp(READOUT_IF_POWER),
                        "resonator",
                        None,
                        dual_demod.full("cosine", "out1", "sine", "out2", I),
                        dual_demod.full("minus_sine", "out1", "cosine", "out2",
                                        Q),
                    )

                    wait(WAIT_BETWEEN * unit_tools.ns, "resonator")

                    save(I, I_st)
                    save(Q, Q_st)
                save(n, n_st)

            with stream_processing():
                I_st.buffer(len(IDLE_TIME)).average().save("I")
                Q_st.buffer(len(IDLE_TIME)).average().save("Q")
                n_st.save("iteration")

        return t1

    def save_measurement(self,
                         save_data: bool = True,
                         quick_plot: bool = True):
        DRIVE_01_FREQUENCY = self.get_settings("sweep.drive_1_if_frequency")[
            0] + self.get_settings("sweep.drive_1_lo_frequency")[0]
        DRIVE_01_IF_POWER = self.get_settings("sweep.drive_1_if_power")[0]
        DRIVE_01_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.qubit_1_pi_wf.samples"))
        DRIVE_01_PULSE_WIDTH = self.get_settings(
            "sweep.drive_1_pulse_width")[0]

        DRIVE_12_FREQUENCY = self.get_settings("sweep.drive_3_if_frequency")[
            0] + self.get_settings("sweep.drive_3_lo_frequency")[0]
        DRIVE_12_IF_POWER = self.get_settings("sweep.drive_3_if_power")[0]
        DRIVE_12_IF_POWER_SCALAR = max(
            self.get_settings("config.waveforms.qubit_3_pi_wf.samples"))
        DRIVE_12_PULSE_WIDTH = self.get_settings(
            "sweep.drive_3_pulse_width")[0]

        READOUT_FREQUENCY = self.get_settings("sweep.readout_if_frequency")[
            0] + self.get_settings("sweep.readout_lo_frequency")[0]
        READOUT_IF_POWER = self.get_settings("sweep.readout_if_power")[0]
        READOUT_IF_POWER_SCALAR = self.get_settings(
            "config.waveforms.ro_wf.sample")
        READOUT_PULSE_WIDTH = self.get_settings("sweep.readout_pulse_width")[0]

        IDLE_TIME = np.array(self.get_settings("sweep.idle_time"))

        job = self.measurement_result
        res_handles = job.result_handles
        res_handles.wait_for_all_values()

        i = unit_tools.demod2volts(
            res_handles.get("I").fetch_all(), READOUT_PULSE_WIDTH)
        q = unit_tools.demod2volts(
            res_handles.get("Q").fetch_all(), READOUT_PULSE_WIDTH)

        complex_signal = i + 1j * q
        readout_powers_in_v = READOUT_IF_POWER * READOUT_IF_POWER_SCALAR

        drive_01_powers_in_v = DRIVE_01_IF_POWER * round(
            DRIVE_01_IF_POWER_SCALAR, 3)
        drive_12_powers_in_v = DRIVE_12_IF_POWER * round(
            DRIVE_12_IF_POWER_SCALAR, 3)

        s21 = complex_signal / readout_powers_in_v  # add axis to divide along readout power axis

        phase = np.angle(complex_signal)

        t1_fit = Fit().T1(IDLE_TIME, np.abs(complex_signal), plot=False)
        # t1_fit = Fit().T1(IDLE_TIME, complex_signal.real, plot=False)

        dims = [
            "idle_time (ns)",
        ]

        dataset = xr.Dataset(
            data_vars={
                "real [V]": (dims, i),
                "imaginary [V]": (dims, q),
                "magnitude [V]": (dims, np.abs(complex_signal)),
                "fitted_magnitude [V]": (dims, t1_fit["fit_func"](IDLE_TIME)),
                "phase [rad]": (dims, phase),
                "s21_real [_]": (dims, s21.real),
                "s21_imaginary [_]": (dims, s21.imag),
                "s21_magnitude [dB]": (dims, x_to_db(np.abs(s21))),
            },
            coords={
                "idle_time (ns)": (("idle_time (ns)", IDLE_TIME)),
            },
            attrs={
                "T1": np.abs(t1_fit["T1"][0]),
                **asdict(self.log_settings)
            },
        ).squeeze()

        fig, ax1 = plt.subplots(1, 1, figsize=(8, 8))

        fig.suptitle("\n".join([
            "T1",
            f"drive_01_frequency = {DRIVE_01_FREQUENCY:.4g} Hz",
            f"drive_01_amplitude = {drive_01_powers_in_v:.4f} V",
            f"drive_01_pulse_width = {DRIVE_01_PULSE_WIDTH} ns",
            "",
            f"drive_12_frequency = {DRIVE_12_FREQUENCY:.4g} Hz",
            f"drive_12_amplitude = {drive_12_powers_in_v:.4f} V",
            f"drive_12_pulse_width = {DRIVE_12_PULSE_WIDTH} ns",
            "",
            f"readout_frequency = {READOUT_FREQUENCY:.4g} Hz",
            f"readout_amplitude = {readout_powers_in_v:.4f} V",
            f"readout_pulse_width = {READOUT_PULSE_WIDTH} ns",
        ]))

        ax1.scatter(IDLE_TIME, np.abs(complex_signal), marker=".")
        # ax1.scatter(IDLE_TIME, complex_signal.real, marker=".")

        ax1.plot(
            IDLE_TIME,
            t1_fit["fit_func"](IDLE_TIME),
            color="tab:orange",
            label=f'T1: {np.abs(t1_fit["T1"][0]):.2f} ns',
        )

        ax1.legend(loc="upper right")

        ax1.set_xlabel("Idle Time [ns]")
        ax1.set_ylabel("|I + iQ| [V]")

        fig.tight_layout()

        if save_data:
            dataset.to_netcdf(
                f"{self.output_path}/{self.name}/{self.name}.hdf5")
            plt.savefig(f"{self.output_path}/{self.name}/{self.name}.png")

        if quick_plot:
            plt.show()

        return dataset


if __name__ == "__main__":
    pass
