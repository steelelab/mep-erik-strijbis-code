from setuptools import os, setup

path = os.path.dirname(__file__)

with open(f'{path}/requirements.txt', 'r', encoding='utf-8') as f:
    requirements = f.readlines()


def package_files(directory):
    paths = []
    for (path, _, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
    return paths


package_data = package_files(os.path.dirname(__file__))

setup(name='eriks_toolbox',
      version='1.0',
      license='',
      author='Erik W. Strijbis',
      author_email='erik.strijbis@gmail.com',
      description=("Code Erik wrote during his MEP. \
     Install using the pip editable package command: python -m pip install -e ."
                   ),
      package_dir={'': 'eriks-toolbox'},
      packages=['eriks_toolbox'],
      include_package_data=True,
      package_data={'eriks_stlab_toolbox': package_data},
      install_requires=requirements,
      classifiers=[
          'Intended Audience :: Science/Research',
          'Programming Language :: Python :: 3.9.18'
      ],
      keywords='superconductiong quantum device characterization scripting',
      url='/'.join(
          ['https:/', 'gitlab.tudelft.nl', 'steelelab', 'mep-erik-strijbis-code']))


